#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include "uhal/tests/tools.hpp"


FILE * logfil=stdout;


using namespace uhal;


uhal::HwInterface *uhal_hw;
ConnectionManager *uhal_manager=0;
uint32_t pll_min,pll_max;

int deint=1;


uint32_t read_ipbus( uhal::HwInterface *hw, const std::string &node_name )
{
        ValWord<uint32_t> value = hw->getNode( node_name ).read();
        hw->dispatch();

        return value.value();
}

void write_ipbus( uhal::HwInterface *hw, const std::string &node, uint32_t value )
{
        hw->getNode( node ).write( value );
        hw->dispatch();
}

void readBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, unsigned nwords, void *buffer )
{
        uhal::ValVector<uint32_t> values = hw->getNode( node_name ).readBlock( nwords );
        hw->dispatch();
        for( unsigned i=0; i < nwords; i++ ){
                ( (uint32_t*)buffer)[i] = values.at(i);
        }
}

void writeBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, const std::vector<uint32_t> &data )
{
        hw->getNode( node_name ).writeBlock( data );
        hw->dispatch();
}




void setup() {
  if (uhal_manager) {
    fprintf(logfil,"otr_setup called more than once\n");
    return;
  }

  uhal::setLogLevelTo(uhal::Error());

  uhal_manager=new ConnectionManager("file://connections.xml");

  static uhal::HwInterface hw1 = uhal_manager->getDevice("FC7");

  uhal_hw=&hw1;
}



int i2c_initialized[4]={0,0,0,0};
uint32_t i2c_reply_addr,i2c_command_addr_w,i2c_settings_addr_w;

//mem:  ral mode
//m16b: ext mode
//ral=1, wr=1 -> write reg_addr and wrdata
//ral=1, wr=0 -> write reg_addr, restart and read one byte
//ext=1, wr=0 -> write reg_addr, restart and read 2 bytes
//wr=1 -> write wrdata
//wr=0 -> read one byte (but documentation says read 2, change?


//return value:
//31:28 ack error 4:1
//27: error
//26 done?
//25:23: m16b,mem,wr
//22:16: addr
//15:8 ext rddata
//7:0 rd data

uint32_t i2c_fmc(HwInterface *hw, uint32_t slave_addr, uint32_t wr, uint32_t reg_addr, uint32_t wrdata, uint32_t mem, uint32_t m16b ) {
 uint32_t status;
 uint32_t attempts,max_attempts=100;
 uint32_t comm;
 ValWord< uint32_t > Reg;
 ClientInterface* c = &hw->getClient();

// uint32_t reply_addr;
// reply_addr  = hw->getNode ( "fmc_i2c_reply_r" ).getAddress();


 comm    = m16b*(1<<25) + mem*(1<<24) + wr*(1<<23) + slave_addr *(1<<16) + reg_addr *(1<<8) + wrdata;

// hw->getNode ("fmc_i2c_command_w").write(comm);
// hw->dispatch();
// hw->getNode ("fmc_i2c_command_w").write(comm| 0x80000000);
// hw->dispatch();
 c->write(i2c_command_addr_w,comm);
 c->dispatch();
 c->write(i2c_command_addr_w,comm| 0x80000000);
 c->dispatch();

 ValWord< uint32_t > reg ;

 status	 = 0;
 attempts = 0;

 while (((status&0xc000000) == 0) && ( attempts <= max_attempts)) {
//   Reg = hw->getNode ( "fmc_i2c_reply_r" ).read();
//   hw->dispatch();
//   status=Reg.value();
   Reg=c->read(i2c_reply_addr);
   c->dispatch();
   status=Reg.value();


   attempts = attempts +1;
 }

// printf("i2c_fmc: %x %d\n",status, attempts);

//	if debug==1:
//		if wr==1 and m16b==0: print "-> slave",'%02x' % slave_addr, "wrdata", '%02x' % wrdata, status, text
//		if wr==1 and m16b==1: print "-> slave",'%02x' % slave_addr, "wrdata", '%04x' % wrdata, status, text
//		if wr==0 and m16b==0: print "-> slave",'%02x' % slave_addr, "rddata", '%02x' % rddata, status, text
//		if wr==0 and m16b==1: print "-> slave",'%02x' % slave_addr, "rddata", '%04x' % rddata, status, text

 usleep(10000);
 return status;
}






void i2c_init(int bus) {

//switch addresses to the most recently initialized bus
//  if (i2c_initialized[bus]) return;



  switch(bus) {
    case 0:
      i2c_reply_addr  = uhal_hw->getNode ( "i2c0_reply_r" ).getAddress();
      i2c_command_addr_w = uhal_hw->getNode ( "i2c0_command_w" ).getAddress();
      write_ipbus(uhal_hw,"i2c0_settings_w",0x804d);
      break;

    case 1:
      i2c_reply_addr  = uhal_hw->getNode ( "i2c1_reply_r" ).getAddress();
      i2c_command_addr_w = uhal_hw->getNode ( "i2c1_command_w" ).getAddress();
      write_ipbus(uhal_hw,"i2c1_settings_w",0x804d);
      break;

    case 2:
      i2c_reply_addr     = uhal_hw->getNode ( "i2cA_reply_r" ).getAddress();
      i2c_settings_addr_w     = uhal_hw->getNode ( "i2cA_settings_w" ).getAddress();
      i2c_command_addr_w = uhal_hw->getNode ( "i2cA_command_w" ).getAddress();
      break;
      
    case 3:
      i2c_reply_addr     = uhal_hw->getNode ( "i2cL_reply_r" ).getAddress();
      i2c_settings_addr_w     = uhal_hw->getNode ( "i2cL_settings_w" ).getAddress();
      i2c_command_addr_w = uhal_hw->getNode ( "i2cL_command_w" ).getAddress();      
      break;      
  }


  i2c_initialized[bus]=1;
}





extern uhal::HwInterface *uhal_hw;
//extern ConnectionManager *uhal_manager;

int main(int argc, char ** argv) {
 unsigned int D0;
 char *p2,*p3;
 int l;

 char dump_dest[256]="";
 int  opt;

 setup();	//initialize IPbus

 while ((opt = getopt(argc, argv, "id:I:R:r:W:D:w:")) != -1) {
  switch (opt) {
    case 'i': 	//initialise
//      setup();
      break;
      
    
    case 'd':
      
      strcpy(dump_dest,optarg);
      break;  
      
    case 'w':
      usleep(atoi(optarg));
      break;  
      
      
   case 'I':

    {

      uint32_t slave_addr,wr,reg_addr,wr_data,mem,m16b;
//      ValWord< uint32_t > reg = hw.getNode ( "fmc_i2c_settings_r" ).read();  
      
//      i2c_init();

      strtok(optarg,",");
      
      
      int i2c_bus=atoi(optarg);
      
      i2c_init(i2c_bus); 

      p2=strtok(0,"=");

      p3=strtok(0,":");
      
      slave_addr=strtol(p2,0,0);
      if (!p3) {
        wr=0; 
        reg_addr=0;
        wr_data=0;
        mem=0;
      } else {
        wr=1;
        reg_addr=strtol(p3,0,0);         
        p2=strtok(0,":");
        if (p2) {
          wr_data=strtol(p2,0,0);
          mem=1;
        } else {
          wr_data=reg_addr;
          reg_addr=0;
          mem=0;
        }  
         m16b=0;
      }
      
//      printf("slave addr: 0x%x wr: 0x%x, reg_addr: 0x%x wr_data: 0x%x mem: 0x%x m16b: 0x%x\n",slave_addr,wr,reg_addr,wr_data,mem,m16b);          

      
      
//wr=1 to write  
//mem=0 to write single byte from wr_data
//mem=1 to write 2 bytes, first byte in reg_addr, second in wr_data  


      uint32_t d=0;
      
      switch(i2c_bus) {
        case 0:
          d=i2c_fmc(uhal_hw,slave_addr, wr, reg_addr, wr_data , mem , m16b   );	      
          break;
          
      
      }    
      
      if ((d&0xc000000)!=0x4000000) printf("I2C error: 0x%x\n",d); else {
        if (wr==0) printf("%x\n",d&0xff);
      }
      
     }
     break;         
   
     

   case 'R': 	//dump register in HEX
    strtok(optarg,":");
    p2=strtok(NULL,"");
    if (p2) { //read multiple entries, e.g. to dump a fifo
      l=strtol(p2,0,0);        
      uint32_t *dbuf=(uint32_t*)malloc(4*l);
      readBlock_ipbus(uhal_hw,optarg,l,dbuf);
      for(int i=0;i<l;i++) printf("%08x\n",dbuf[i]);
      free(dbuf);
    } else printf("0x%x\n",read_ipbus(uhal_hw,optarg));
    break;

   case 'r': 	//dump register in decimal
    printf("%d\n",read_ipbus(uhal_hw,optarg));
    break;    
    
   case 'W': 	//write register
    strtok(optarg,"=");
    p2=strtok(0,"=");

    D0=strtol(p2,0,0);
    write_ipbus(uhal_hw,optarg,D0); 
    break;         
      
      
      
  }    
 }
 return 0;
}