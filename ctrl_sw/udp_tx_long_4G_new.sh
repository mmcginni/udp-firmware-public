#!/bin/bash

./fc7_ctrl -W gbt_if.ctrl=0x01010100

(cat > /dev/null) <<EOF
./fc7_ctrl -W gbt_if.header_addr=0 -W gbt_if.header_data=0x941ef2f8
./fc7_ctrl -W gbt_if.header_addr=1 -W gbt_if.header_data=0xbbaaf860
./fc7_ctrl -W gbt_if.header_addr=2 -W gbt_if.header_data=0x52efddcc
./fc7_ctrl -W gbt_if.header_addr=3 -W gbt_if.header_data=0x00450008
./fc7_ctrl -W gbt_if.header_addr=4 -W gbt_if.header_data=0x002a1820
./fc7_ctrl -W gbt_if.header_addr=5 -W gbt_if.header_data=0x11ff0040
./fc7_ctrl -W gbt_if.header_addr=6 -W gbt_if.header_data=0xa8c02fae
./fc7_ctrl -W gbt_if.header_addr=7 -W gbt_if.header_data=0xa8c05201
./fc7_ctrl -W gbt_if.header_addr=8 -W gbt_if.header_data=0x29090201
./fc7_ctrl -W gbt_if.header_addr=9 -W gbt_if.header_data=0x04202909
./fc7_ctrl -W gbt_if.header_addr=10 -W gbt_if.header_data=0x00000000
EOF

#3071 <- nov10 was using this
(cat > /dev/null) <<EOF
./fc7_ctrl -W gbt_if.header_addr=0 -W gbt_if.header_data=0x941ef2f8
./fc7_ctrl -W gbt_if.header_addr=1 -W gbt_if.header_data=0xbbaaf860
./fc7_ctrl -W gbt_if.header_addr=2 -W gbt_if.header_data=0x52efddcc
./fc7_ctrl -W gbt_if.header_addr=3 -W gbt_if.header_data=0x00450008
./fc7_ctrl -W gbt_if.header_addr=4 -W gbt_if.header_data=0x002a1830
./fc7_ctrl -W gbt_if.header_addr=5 -W gbt_if.header_data=0x11ff0040
./fc7_ctrl -W gbt_if.header_addr=6 -W gbt_if.header_data=0xa8c02f9e
./fc7_ctrl -W gbt_if.header_addr=7 -W gbt_if.header_data=0xa8c05201
./fc7_ctrl -W gbt_if.header_addr=8 -W gbt_if.header_data=0x29090201
./fc7_ctrl -W gbt_if.header_addr=9 -W gbt_if.header_data=0x04302909
./fc7_ctrl -W gbt_if.header_addr=10 -W gbt_if.header_data=0x00000000
EOF

#4095
(cat > /dev/null) <<EOF
./fc7_ctrl -W gbt_if.header_addr=0 -W gbt_if.header_data=0x941ef2f8
./fc7_ctrl -W gbt_if.header_addr=1 -W gbt_if.header_data=0xbbaaf860
./fc7_ctrl -W gbt_if.header_addr=2 -W gbt_if.header_data=0x52efddcc
./fc7_ctrl -W gbt_if.header_addr=3 -W gbt_if.header_data=0x00450008
./fc7_ctrl -W gbt_if.header_addr=4 -W gbt_if.header_data=0x002a1840
./fc7_ctrl -W gbt_if.header_addr=5 -W gbt_if.header_data=0x11ff0040
./fc7_ctrl -W gbt_if.header_addr=6 -W gbt_if.header_data=0xa8c02f8e
./fc7_ctrl -W gbt_if.header_addr=7 -W gbt_if.header_data=0xa8c05201
./fc7_ctrl -W gbt_if.header_addr=8 -W gbt_if.header_data=0x29090201
./fc7_ctrl -W gbt_if.header_addr=9 -W gbt_if.header_data=0x04402909
./fc7_ctrl -W gbt_if.header_addr=10 -W gbt_if.header_data=0x00000000
EOF

#nov10
./fc7_ctrl -W gbt_if.header_addr=0 -W gbt_if.header_data=0x941ef2f8
./fc7_ctrl -W gbt_if.header_addr=1 -W gbt_if.header_data=0xbbaaf860
./fc7_ctrl -W gbt_if.header_addr=2 -W gbt_if.header_data=0x52efddcc
./fc7_ctrl -W gbt_if.header_addr=3 -W gbt_if.header_data=0x00450008
./fc7_ctrl -W gbt_if.header_addr=4 -W gbt_if.header_data=0x002a4030
./fc7_ctrl -W gbt_if.header_addr=5 -W gbt_if.header_data=0x11ff0040
./fc7_ctrl -W gbt_if.header_addr=6 -W gbt_if.header_data=0xa8c0079e
./fc7_ctrl -W gbt_if.header_addr=7 -W gbt_if.header_data=0xa8c05201
./fc7_ctrl -W gbt_if.header_addr=8 -W gbt_if.header_data=0x29090201
./fc7_ctrl -W gbt_if.header_addr=9 -W gbt_if.header_data=0x2c302909
./fc7_ctrl -W gbt_if.header_addr=10 -W gbt_if.header_data=0x00000000






#number of packets and delay between packets
./fc7_ctrl -W gbt_if.tx_pkt_n=4000000000 -W gbt_if.tx_pkt_w=100

#payload size in 32-bit words, also generates strobe which starts packet generator
#./fc7_ctrl -W gbt_if.payload_len=2047
# <- Nov 10 was using this ./fc7_ctrl -W gbt_if.payload_len=3071
#./fc7_ctrl -W gbt_if.payload_len=4095

./fc7_ctrl -W gbt_if.payload_len=3081
