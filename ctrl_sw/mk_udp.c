//found online, probably indirectly from tcpdump?

#include <stdio.h>
//#include <stdlib.h>		// free()
//#include <sys/types.h>		// sendto()
//#include <sys/socket.h>		// sendto()
#include <string.h>
#include <netinet/in.h>
#include <linux/if_ether.h>

#define IPHDRSIZE     sizeof(struct iphdr)
#include <netinet/in.h>

#define IPVERSION 4

struct iphdr {
  u_char  ihl:4,        /* header length */
          version:4;    /* version */
  u_char  tos;          /* type of service */
  short   tot_len;      /* total length */
  u_short id;           /* identification */
  short   off;          /* fragment offset field */
#define IP_DF   0x4000  /* dont fragment flag */
#define IP_MF   0x2000  /* more fragments flag */
  u_char  ttl;          /* time to live */
  u_char  protocol;     /* protocol */
  u_short check;        /* checksum */
  struct  in_addr saddr, daddr;  /* source and dest address */
};


#define UDPHDRSIZE    sizeof(struct udphdr)
#define MAX_UDP_PACKET 1024

struct udphdr {
        u_short source;         /* source port */
        u_short dest;                   /* destination port */
        u_short len;                    /* udp length */
        u_short check;          /* udp checksum */
};



unsigned short in_cksum(unsigned short *ptr, int nbytes) {

    register long sum; /* assumes long == 32 bits */
    u_short oddbyte;
    register u_short answer; /* assumes u_short == 16 bits */
    /*
     * the algorithm is simple, using a 32-bit accumulator (sum),
     * we add sequential 16-bit words to it, and at the end, fold back
     * all the carry bits from the top 16 bits into the lower 16 bits.
     */
    sum = 0;
    while (nbytes > 1) {
        sum += *ptr++;
        nbytes -= 2;
    }

    /* mop up an odd byte, if necessary */
    if (nbytes == 1) {
        oddbyte = 0; /* make sure top half is zero */
        *((u_char *) &oddbyte) = *(u_char *) ptr; /* one byte only */
        sum += oddbyte;
    }

    /*
     * Add back carry outs from top 16 bits to low 16 bits.
     */
    sum = (sum >> 16) + (sum & 0xffff); /* add high-16 to low-16 */
    sum += (sum >> 16); /* add carry */
    answer = ~sum; /* ones-complement, then truncate to 16 bits */
    return (answer);
}




int udp_header(uint8_t *packet, uint64_t s_hw, uint64_t d_hw,unsigned long saddr, unsigned long daddr,unsigned short sport,unsigned short dport,unsigned datasize) {
        unsigned int i;
        int j;
        
        uint64_t n_hw;	
        struct ethhdr		eth_hdr;
        struct iphdr		ip_head;
        struct udphdr		udp_head;      
       

        n_hw=htobe64(d_hw);
        memcpy(eth_hdr.h_dest,(char *)&n_hw+sizeof(n_hw)-ETH_ALEN,ETH_ALEN);

        n_hw=htobe64(s_hw);
        memcpy(eth_hdr.h_source,(char *)&n_hw+sizeof(n_hw)-ETH_ALEN,ETH_ALEN);

        eth_hdr.h_proto=htons(ETH_P_IP);

        ip_head.version  	= 4;
        ip_head.ihl		= IPHDRSIZE/4;
        ip_head.saddr.s_addr	= htonl(saddr);
        ip_head.daddr.s_addr	= htonl(daddr);
        ip_head.ttl		= 255;
        ip_head.id		= 42;
        ip_head.off             = htons(IP_DF);
        ip_head.protocol	= IPPROTO_UDP;
        ip_head.tot_len		= htons(IPHDRSIZE + UDPHDRSIZE + datasize);
        ip_head.check		= 0;
        ip_head.check		= in_cksum((u_short *)&ip_head,IPHDRSIZE);

	udp_head.source		= htons(sport);
	udp_head.dest		= htons(dport);
	udp_head.len		= htons(UDPHDRSIZE+datasize);
	udp_head.check		= 0;


        memcpy(packet,(char*)&eth_hdr,ETH_HLEN);
	memcpy(packet+ETH_HLEN, (char *)&ip_head, IPHDRSIZE);
	memcpy(packet+ETH_HLEN+IPHDRSIZE, (char *)&udp_head, UDPHDRSIZE);

#ifdef DUMP
 printf("ip_head.version: %x\n", ip_head.version);
 printf("ip_head.ihl: %x\n",ip_head.ihl);
 printf("ip_head.saddr.s_addr: %x\n", ip_head.saddr.s_addr);
 printf("ip_head.daddr.s_addr: %x\n",ip_head.daddr.s_addr);
 printf("ip_head.ttl: %x\n",ip_head.ttl);
 printf("ip_head.id: %x\n",ip_head.id);
 printf("ip_head.off: %x\n",ip_head.off);
 printf("ip_head.protocol: %x\n",ip_head.protocol);
 printf("ip_head.tot_len: %d\n",ntohs(ip_head.tot_len));
 printf("ip_head.check: %x\n",ip_head.check);
 printf("ip_head.check: %x\n", in_cksum((u_short *)&ip_head,IPHDRSIZE));
 printf("udp_head.source: %d\n",ntohs(udp_head.source));
 printf("udp_head.dest: %d\n",ntohs(udp_head.dest));
 printf("udp_head.len: %d\n",ntohs(udp_head.len));
 printf("udp_head.check: %x\n",udp_head.check);
#endif
 
	

        return ETH_HLEN+IPHDRSIZE+UDPHDRSIZE;	

       
}


#ifdef STANDALONE

int main (int argc, char ** argv) {
 uint8_t packet[ETH_HLEN+IPHDRSIZE+UDPHDRSIZE];
 unsigned int i;
 int j;

 // udp_header(packet,0xaabbccddef52,0xf8f21e9460f8,0xC0A80152,0xC0A80102,2345,2345,2047*4);

 // nov 10, adding words, should have been  3071?
 //   udp_header(packet,0xaabbccddef52,0xf8f21e9460f8,0xC0A80152,0xC0A80102,2345,2345,4095*4);
  udp_header(packet,0xaabbccddef52,0xf8f21e9460f8,0xC0A80152,0xC0A80102,2345,2345,(10+3071)*4); 
#if 0 
 for(i=0;i<ETH_HLEN+IPHDRSIZE+UDPHDRSIZE;i++) {
   if (i==ETH_HLEN+IPHDRSIZE+UDPHDRSIZE-1) printf("1"); else printf("0");
     for(j=7;j>=0;j--) printf("%d",(packet[i]>>j)&1);
     printf(",\n");

 } 
#endif 
 
#if 1
 for(i=0;i<ETH_HLEN+IPHDRSIZE+UDPHDRSIZE;i+=4) {
     printf("./fc7_ctrl -W gbt_if.header_addr=%d -W gbt_if.header_data=0x%02x%02x%02x%02x\n",i/4,packet[i+3],packet[i+2],packet[i+1],packet[i+0]);

 } 
#endif 



 return 0;
}

#endif
