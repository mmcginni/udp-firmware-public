#!/bin/sh

#grep 192.168.1.82 iftop.dump  | awk '{print $3}' | sed -e 's,\([[:digit:]]*\)\([[:alpha:]]\),\1 \2,' > tmp.log
grep 192.168.1.82 iftop_vary.dump  | awk '{print $3}' | sed -e 's,\([[:digit:]]*\)\([[:alpha:]]\),\1 \2,' > tmp.log

cat > makePauseGraph.C <<EOF
int makePauseGraph()  {
  vector<float> index,rate;

  //-----
EOF


idx=0;
while read line ; do

    #echo $line

    gbps=`echo $line | awk '{print $1}'`;
    unit=`echo $line | awk '{print $2}'`;    

    if [ "x$unit" == "xMb" ]; then
	gbps=`echo "$gbps/1000" | bc -l`
    elif [ "x$unit" == "xKb" ]; then
	gbps=`echo "$gbps/1000/1000" | bc -l`	
    elif [ "x$unit" == "xb" ]; then
	gbps=`echo "$gbps/1000/1000/1000" | bc -l`	
    fi;

    echo "index.push_back($idx); rate.push_back($gbps);" >> makePauseGraph.C
    #echo ""
    (( idx++ ));
    
done < <(cat tmp.log)

cat >> makePauseGraph.C <<EOF
  //-----  

  //  TFile * f = new TFile("pause.root","recreate");
  TFile * f = new TFile("pause_vary.root","recreate");
  TGraph * g = new TGraph(index.size(),(float*)&(index[0]),(float*)&(rate[0]));
  g->Write("pause");
  f->Close();

    return 0;
}
EOF
