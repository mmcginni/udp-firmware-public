#!/bin/bash
#based on https://access.redhat.com/sites/default/files/attachments/201501-perf-brief-low-latency-tuning-rhel7-v2.1.pdf

systemctl stop firewalld
systemctl disable firewalld
iptables -F ; iptables -t nat -F; iptables -t mangle -F ; ip6tables -F
iptables -X ; iptables -t nat -X; iptables -t mangle -X ; ip6tables -X
iptables -t raw -F ; iptables -t raw -X
#modprobemodprobe-r-rebtable_nat ebtables
modprobe -r ebtable_nat ebtables
modprobe -r ipt_SYNPROXY nf_synproxy_core xt_CT \
nf_conntrack_ftp nf_conntrack_tftp nf_conntrack_irc \
nf_nat_tftp ipt_MASQUERADE iptable_nat nf_nat_ipv4 \
nf_nat nf_conntrack_ipv4 nf_nat nf_conntrack_ipv6 \
xt_state xt_conntrack iptable_raw nf_conntrack \
iptable_filter iptable_raw iptable_mangle ipt_REJECT \
xt_CHECKSUM ip_tables nf_defrag_ipv4 ip6table_filter \
ip6_tables nf_defrag_ipv6 ip6t_REJECT xt_LOG \
xt_multiport nf_conntrack

#tuned-adm profile network-latency
killall tuned
#tuned &
