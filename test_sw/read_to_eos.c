#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <signal.h>
#include <sched.h>

#include <arpa/inet.h>
#include <sys/socket.h>


#include <sys/time.h>
#include <sched.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>

#ifdef XRD
#include <cppunit/extensions/HelperMacros.h>
#include "XrdCl/XrdClFile.hh"
#include "CppUnitXrdHelpers.hh"

using namespace XrdCl;
//using namespace XrdClTests;
#endif

/* #define BUFSIZE (8192+512)   */
/* #define BUFNUM  8192 */
#define BUFSIZE (12296+512)   //bytes
//#define BUFNUM  8192
#define BUFNUM  262144UL
#define CHUNK   2048

#define XRDCHUNKS 16

uint8_t *buf;

pthread_mutex_t recv_mutex;
pthread_cond_t  recv_cond;

uint32_t run=1;
volatile uint64_t r_pos=0,w_pos=0;

uint64_t data_sum=0,packet_count=0,max_buf_use=0;
unsigned int mismatch=0,lost=0;
uint64_t t_start, t_start_kh, t_stop_kh;

sem_t * sem[BUFNUM/CHUNK]; // 128
int the_sem_no=0;

double gltime() {
 struct timeval tv;
  
 gettimeofday(&tv,0);
 return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}

void sighandler_int(int a) {
 printf("SIGINT\n");
 sem_post(sem[the_sem_no]);
 sem_close(sem[the_sem_no]);
 exit(0);
}


int main(int argc, char *argv[]) {

  pthread_t recv_thread_id;
  uint16_t pktidx,pktidx_prev;
  uint8_t *p;
  uint32_t d;

  signal(SIGINT,sighandler_int);

  

  fprintf(stderr,"starting...\n");

  int count=0, first_time=1, mismatch=0,wrote=0;;
  unsigned last_counter;
  uint64_t tstart[BUFNUM/CHUNK], tmid[BUFNUM/CHUNK], tend[BUFNUM/CHUNK];
  uint64_t tstartOH[BUFNUM/CHUNK], tendOH[BUFNUM/CHUNK];  
  
  char sname[256];
  char * temp = (char *)malloc(BUFSIZE*CHUNK);

    /* sprintf(sname, "/mnt/ramdisk/udp_0.dat"); */
    /* int ffdsrc = open(sname,  O_RDONLY |O_SYNC , 00666); //| O_NOATIME */
    /* FILE * ffsrc = fdopen(ffdsrc, "r"); */
  
#ifdef FUSE  
  // simple fuse test ...
  int ffdsrc[128],ffddest[128];
  FILE *ffsrc[128], *ffdest[128]; 
  for( int i=0; i<128; i++ ) {
    sprintf(sname, "/mnt/ramdisk/udp_%d.dat", i);
    ffdsrc[i] = open(sname,  O_RDONLY , 00666); //| O_NOATIME |O_SYNC 
    ffsrc[i] = fdopen(ffdsrc[i], "r");
    sprintf(sname, "/eos/www/udp_%d.dat",i);
    ffddest[i] = open(sname,  O_CREAT| O_WRONLY , 00666); //| O_NOATIME | O_SYNC 
    ffdest[i] = fdopen(ffddest[i], "w");
  }
  for( int i=0; i<128; i++ ) { 
    tstartOH[i]=gltime();      
    /* sprintf(sname, "/mnt/ramdisk/udp_%d.dat", i); */
    /* int ffdsrc = open(sname,  O_RDONLY |O_SYNC , 00666); //| O_NOATIME */
    /* FILE * ffsrc = fdopen(ffdsrc, "r"); */

    tstart[i] = gltime();
    fread(temp, sizeof(char), BUFSIZE*CHUNK, ffsrc[i]);
    tmid[i] = gltime();
    /* sprintf(sname, "/eos/www/udp_%d.dat",i); */
    /* //    sprintf(sname, "/home/udptester/eos/www/udp_%d.dat",i); */
    /* int ffddest = open(sname,  O_CREAT| O_WRONLY | O_SYNC , 00666); //| O_NOATIME */
    /* FILE * ffdest = fdopen(ffddest, "w"); */
    fwrite(temp, sizeof(char), BUFSIZE*CHUNK, ffdest[i]);
    tend[i] = gltime();

    printf("%d\t%lu\t%lu\t%lu\n",i,tstart[i],tmid[i],tend[i]);
    

    tendOH[i]=gltime();      
  }
  for( int i=0; i<128; i++ ) {
    fclose(ffsrc[i]);
    fclose(ffdest[i]);
    close(ffdsrc[i]);
    close(ffddest[i]);    
  }
#endif



  
#ifdef XRD
  File f1;

#ifdef IOVEC
  int iovcnt = 2;
  iovec iov[iovcnt];
  iov[0].iov_base = temp;
  iov[0].iov_len  = BUFSIZE*CHUNK/2;
  iov[1].iov_base = temp+(CHUNK/2);
  iov[1].iov_len  = BUFSIZE*CHUNK/2;
#endif

#ifdef CHUNKVEC
  XrdCl::ChunkList chunks;
#endif
  
  for( int i=0; i<128; i++ ) { 
    sprintf(sname, "/mnt/ramdisk/udp_%d.dat", i);
    int ffdsrc = open(sname,  O_RDONLY |O_SYNC , 00666); //| O_NOATIME
    FILE * ffsrc = fdopen(ffdsrc, "r");
    sprintf(sname, "/udp_%d.dat",i);
    std::string address = "root://eosuser.cern.ch:1094/";
    std::string dataPath = "/eos/user/k/khahn/www";
    std::string filePath = dataPath + sname;
    std::string fileUrl = address;
    fileUrl += filePath;
    CPPUNIT_ASSERT( f1.Open( fileUrl, OpenFlags::Delete | OpenFlags::Update,
			     Access::UR | Access::UW ).IsOK() );


    // the read ------------------------------
    tstart[i] = gltime();
    fread(temp, sizeof(char), BUFSIZE*CHUNK, ffsrc);
    tmid[i] = gltime();
#ifdef CHUNKVEC
    for( int j=0; j<XRDCHUNKS; j++ ) {
      chunks.push_back( XrdCl::ChunkInfo( j*(BUFSIZE*CHUNK)/XRDCHUNKS, (BUFSIZE*CHUNK)/XRDCHUNKS, temp ) );
    }
#endif
    
    // the write -----------------------------

    //cout << "opened ... " << endl;
#ifdef IOVEC
    CPPUNIT_ASSERT( f1.WriteV( 0, iov, iovcnt ).IsOK() );
#elif CHUNKVEC
    CPPUNIT_ASSERT_XRDST( f1.VectorWrite( chunks ) );
    chunks.clear();
#else
    CPPUNIT_ASSERT( f1.Write( 0, BUFSIZE*CHUNK, temp ).IsOK() );
#endif
    //  cout << "wrote 4MB ... " << endl;
    //    CPPUNIT_ASSERT( f1.Sync().IsOK() );
    //cout << "sync ... " << endl;
    tend[i] = gltime();

    printf("%d\t%lu\t%lu\t%lu\n",i,tstart[i],tmid[i],tend[i]);
    
    fclose(ffsrc);
    CPPUNIT_ASSERT( f1.Close().IsOK() );
  }
#endif


  uint64_t avgr=0,avgw=0,avgrw=0,avgwOH=0;
  for(int i=0; i<128; i++ ) {
    avgr += (tmid[i]-tstart[i]);
    avgw += (tend[i]-tmid[i]);
    avgrw += (tend[i]-tstart[i]);    
    avgwOH += (tendOH[i]-tstart[i]);
  }
  double totmb=(BUFSIZE*CHUNK)/(1024.*1024.);
  double rsecs = (avgr)/(1000000.*(128));
  double wsecs = (avgw)/(1000000.*(128));
  double rwsecs = (avgrw)/(1000000.*(128));  
  double wsecsOH = (avgwOH)/(1000000.*(BUFNUM/CHUNK));    
  uint64_t tstamp=gltime();
    printf( "size: %.1f MB, time: %f\tread rate: %.3f GB/s, write rate: %.3f GB/s\ttime: %llu\trateRW: %f\trateOH: %f\n",
	    totmb,
	    rsecs,
	    totmb/rsecs/1024,
	    totmb/wsecs/1024,
	    tstamp,
	    totmb/rwsecs/1024,
	    totmb/wsecsOH/1024);	      

  return 0;


  

  while(1) { 

    for( int i=0; i<(BUFNUM/CHUNK); i++ ) {
      tstartOH[i]=gltime();
      sprintf(sname,"/ramdisk-semaphore-%d", i);
      sem[i] = sem_open(sname, O_RDWR);
      //      printf("waiting on %s\n", sname);
      //fflush(stdout);
      sem_wait(sem[i]);
      the_sem_no = i;
      //      printf("locked %s\n", sname);
      fflush(stdout);
      
      sprintf(sname, "/mnt/ramdisk/udp_%d.dat", i);
      int fdsrc = open(sname,  O_RDWR |O_SYNC , 00666); //| O_NOATIME
      FILE * fsrc = fdopen(fdsrc, "r");
      //      printf("opened %s\n",sname);
      sprintf(sname, "/mnt/nvme0/udp_%d.dat", i);
      //sprintf(sname, "/home/udptester/delete_me/udp_%d.dat", i*1000);
      int fddest = open(sname,  O_CREAT| O_RDWR | O_SYNC , 00666); //| O_NOATIME
      FILE * fdest = fdopen(fddest, "w");
      //      printf("opened %s\n",sname);

      //      int idx=count*8+i-1;
      tstart[i]=gltime();
      fread(temp, sizeof(char), BUFSIZE*CHUNK, fsrc);
      tmid[i]=gltime();

      // check for overwrites :
      //1903df0 ff73 ff73 0000 0000 0000 0000 0000 0000
      char * cptr = temp;
      if( !first_time ) { 
	unsigned first_counter = ((0xff&cptr[1]) << 8UL) | (0xff&cptr[0]) ;
	/* printf("%d :: first: 0x%x\tlast: 0x%x\tc0: %x\tc1: %x\n", */
	/*        i, */
	/*        first_counter, */
	/*        last_counter, */
	/*        0xff&cptr[0], */
	/*        0xff&cptr[1]); */
	if( first_counter != last_counter ) {
	  printf("mismatch (%d) :: first: 0x%x\tlast: 0x%x\n",
		 i,
		 first_counter,
		 last_counter );
	  mismatch++;
	}
      } else {
	first_time = 0;
	/* for( int i=0; i<BUFSIZE*CHUNK; i++ ) { */
	/*   if( !(i%2) && i>0 &&(i%16)) printf("_"); */
	/*   if( !(i%16) && i>0) printf("\n"); */
	/*   if( !(i%16) ) printf("%8x\t", i ); */
	/*   printf("%2x",(0xff&temp[i])); */
	/* } */
      }                       
      last_counter = ((0xff&cptr[0x1903de1]) << 8UL) | (0xff&cptr[0x1903de0]);
      //      printf("last: 0x%x\tc0: 0x%x\tc1: 0x%x\n", last_counter, 0xff&cptr[0x1903de0], 0xff&cptr[0x1903de1]);
      
      //      printf("read %d\n",BUFSIZE*CHUNK);
      fwrite(temp, 1, BUFSIZE*CHUNK, fdest);
      tend[i]=gltime();      
      //      printf("wrote\n");
      fclose(fsrc);
      fclose(fdest);
      wrote++;
      int ret = sem_post(sem[i]);
      if ( ret < 0 ) {
	if(errno == EINVAL ) 	printf("sem_post:EINVAL\n");
	if(errno == EOVERFLOW ) 	printf("sem_post:EOVERFLOW\n");	
      }
      //      printf("unlocked %s\n", sname);
      //      printf("-->index: %d\t%lu\t%lu\t%lu\n",idx,tstart[i],tmid[i],tend[i]);
      tendOH[i]=gltime();      
    }

    uint64_t avgr=0,avgw=0,avgwOH=0;
    for(int i=0; i<(BUFNUM/CHUNK); i++ ) {
      avgr += (tmid[i]-tstart[i]);
      avgw += (tend[i]-tmid[i]);
      avgwOH += (tendOH[i]-tstart[i]);
      //      printf("%lu\t%lu\t%lu\t%lu\t%lu\n",tstart[i],tmid[i],tend[i],avgr,avgw);
    }
    double totmb=(BUFSIZE*CHUNK)/(1024.*1024.);
    double rsecs = (avgr)/(1000000.*(BUFNUM/CHUNK));
    double wsecs = (avgw)/(1000000.*(BUFNUM/CHUNK));
    double wsecsOH = (avgwOH)/(1000000.*(BUFNUM/CHUNK));    
    uint64_t tstamp=gltime();
    printf( "size: %.1f MB, time: %f\tread rate: %.3f GB/s, write rate: %.3f GB/s\ttime: %llu\twpkt: %d\trateOH: %f\n",
	    totmb,
	    rsecs,
	    totmb/rsecs/1024,
	    totmb/wsecs/1024,
	    tstamp,
	    wrote,
	    totmb/wsecsOH/1024);	      
    fflush(stdout);
    count = 0;  
  }



}
