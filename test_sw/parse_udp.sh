#!/bin/sh
logfile=$1
rootfile=`echo $logfile | sed -s 's,log,root,'`

while read line; do
    wrate=`echo $line | awk '{print $9}'`
    tstamp=`echo $line | awk '{print $11}'`
    skipped=`echo $line | awk '{print $13}' | sed 's/,//' `
    ovrpkt=`echo $line | awk '{print $15}' | sed  's,/.*,,'`
    totpkt=`echo $line | awk '{print $15}' | sed 's,.*/,,'`
    totgb=`echo $line | awk '{print $16}' | sed 's,(,,;s,GB.*,,'`                

    echo "$wrate:$tstamp:$skipped:$ovrpkt:$totpkt:$totgb"
    
done < <(grep Wrote $logfile) > udp.tmp

root -l -q -b udp.C\(\"udp.tmp\",\"$rootfile\"\)
