#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <signal.h>
#include <sched.h>

#include <arpa/inet.h>
#include <sys/socket.h>


#include <sys/time.h>
#include <sched.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>

#include <sys/ipc.h>
#include <sys/shm.h>

/* Assume a cannot point towards an area larger than 4096 bytes. */
#define A_MAX_SIZE (size_t)4096
int * is_packetready;

//MM: handle_error routines not used
//#define handle_error_en(en, msg) do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

//#define handle_error( msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)

/* #define BUFSIZE (8192+512)   */
/* #define BUFNUM  8192 */
#define BUFSIZE (12296+512)   //bytes
/* #define BUFNUM  65536 */
/* #define CHUNK   512 */
//#define BUFNUM  262144UL

/* #define BUFNUM  262144UL */
/* #define CHUNK   2048 */

// jun24 only 8gb on test machine #define BUFNUM  1048576U
#define BUFNUM  131072UL
#define CHUNK   2048

uint8_t *buf_ptr;

uint32_t run=1;
volatile uint64_t r_pos=0,w_pos=0;

uint64_t total_data_length=0,packet_count=0,max_buf_use=0;
unsigned int mismatched_packets=0,lost_packets=0;
#define NSAMPLES 100
uint64_t t_start, t_start_write[NSAMPLES], t_stop_write[NSAMPLES], t_test;
//uint64_t t_recv[NSAMPLES];       MM: Never used

sem_t * sem_array[BUFNUM/CHUNK];
int sharedmem_id = 0;

//int reset_pointers = 0;           MM:never used
int is_firstindex = 0;

double getTime() {
  struct timeval tv;
  
  gettimeofday(&tv,0);
  return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}



void printStatWritingOnly(int tot_written, int skipped, int tot_overwritten){
  uint64_t diff=0;   //,recv=0;
  for( int sample_index=0; sample_index<NSAMPLES; sample_index++ ) {
    diff += t_stop_write[sample_index] - t_start_write[sample_index];
    //recv += t_recv[sample_index];
  }
  double avgdiff = (double)diff/NSAMPLES;
  //double avgrecv = (double)recv/NSAMPLES;
  double fracamt = (double)BUFSIZE*CHUNK;///(1024*1024*1024); // GB
  double fractime = (avgdiff)/1000000;
  uint64_t tstamp = getTime();
  //printf("tstamp: %llu \n",tstamp/1000000);

  double totgb = (double)(tot_written)*BUFSIZE*CHUNK/(1024.*1024.*1024.);
  printf("Wrote %.3f MB in %.7f s, rate (GB/s): %f\ttime: %llu\tskipped: %d, ovrwrt/tot: %d/%d (%.2f GB)\n",
	 fracamt/1024/1024,
	 fractime,
	 fracamt/(1024*1024*1024*fractime),
	 tstamp,
	 skipped,
	 tot_overwritten,
	 tot_written,
	 totgb);  
  fflush(stdout);    
}



int socketConfig(struct sockaddr_in dst_addr){
  FILE * cache_file = fopen("/proc/sys/vm/drop_caches", "wr");
  int socket_descriptor;
  //  fprintf(stderr,"making socket\n");
  if ((socket_descriptor=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0) {
    perror("socket error");
    exit(-1);
  }
  //  fprintf(stderr,"socket socket_descriptor: %d\n",socket_descriptor);
  memset((char *) &dst_addr, 0, sizeof(dst_addr));
  
  dst_addr.sin_family = AF_INET;
  dst_addr.sin_port = htons(2345);
  
  //KH
  dst_addr.sin_addr.s_addr = inet_addr("192.168.1.2");
  //dst_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  if( bind(socket_descriptor , (struct sockaddr*)&dst_addr, sizeof(dst_addr) ) <0 )   {
    perror("bind error");
  }  
  

  unsigned int rcvbuf_size,rcvbuf_size_len;
  if (getsockopt(socket_descriptor, SOL_SOCKET, SO_RCVBUF, &rcvbuf_size, &rcvbuf_size_len) == -1) {
    perror("getsockopt error");
    //printf("setsockopt failed\n");
  } else printf("recieve buffer size: %d\n",rcvbuf_size);

  rcvbuf_size = 1024 * 1024*512;
  if (setsockopt(socket_descriptor, SOL_SOCKET, SO_RCVBUFFORCE, &rcvbuf_size, sizeof(rcvbuf_size)) == -1) {
    perror("setsockopt error");
    //printf("setsockopt failed\n");
  }

  if (getsockopt(socket_descriptor, SOL_SOCKET, SO_RCVBUF, &rcvbuf_size, &rcvbuf_size_len) == -1) {
    perror("getsockopt error");
    //printf("setsockopt failed\n");
  }  else printf("recieve buffer size:: %d\n",rcvbuf_size);
  
  return socket_descriptor;
}



static void * threadWritetoRamdisk(void *data) {
  struct sockaddr_in src_addr,dst_addr;
  int message_len;
  socklen_t src_len;
  int socket_descriptor=socketConfig(dst_addr);
  
  int sample_index=0,skipped=0;
  //int fidx=0;                   MM: never used
  int first_time=1;
  int tot_overwritten=0, overwritten=0;
  int tot_written=0;
  while(run) {
    src_len=sizeof(src_addr);
    //    fprintf(stderr,"calling recvfrom ..., r_pos=%d, ptr: %lu\n",r_pos,buf_ptr+BUFSIZE*r_pos);
    t_start=getTime();
    
    //    if ((message_len=recvfrom(socket_descriptor, buf_ptr+BUFSIZE*r_pos, BUFSIZE, 0, (struct sockaddr *) &src_addr, &src_len))<0) {
    int waited_period=0;
    while ((message_len=recvfrom(socket_descriptor, buf_ptr+BUFSIZE*r_pos, BUFSIZE, MSG_DONTWAIT, (struct sockaddr *) &src_addr, &src_len))<0) {
      uint64_t t_wait = getTime();
      if( (double)(t_wait - t_start)/1000000 > 5. ) {
	printf("waited 5sec for data ... segments overwritten in prev cycle: %d\n",
	       overwritten );
	overwritten=0;
	fflush(stdout);
	if( !waited_period && packet_count  )  {
	  // flush the last packets to udp_0 ...
	  int index=(r_pos/CHUNK); // no -1
	  char outfile_name[256];
	  sem_wait(sem_array[index]);      
	  sprintf(outfile_name,"/mnt/ramdisk/udp_%d.dat", index);//r_pos); 
	  printf("flushing last packets to %s\n", outfile_name);
	  int outfile_descriptor = open(outfile_name,  O_CREAT | O_WRONLY | O_DSYNC | O_TRUNC , 00666); //| O_NOATIME
	  FILE* outfile_handle = fdopen(outfile_descriptor, "w");
	  fwrite(buf_ptr+BUFSIZE*(r_pos-(r_pos%CHUNK)), BUFSIZE*(r_pos%CHUNK), 1, outfile_handle);
	  fclose( outfile_handle );
	  close(outfile_descriptor);
	  sem_post(sem_array[index]);
	} else {
	  // do this 5s later, igves time for cond above and check catchup
	  r_pos = 0; 
	  w_pos = 0; 
	}
	waited_period++;
	
	//reset_pointers = 1;
	t_start = t_wait;
      }
      continue;
      //    perror("recvfrom");
    }

    //t_recv[sample_index]=getTime()-t_start;
    //printf("first idx: %x last idx: %x\n",packet_index,last_packet_index);
    
    // KH write pkt to disk
    //fidx=r_pos;
    int status_sem_wait;
    if( !(r_pos%CHUNK) && r_pos) { // pktcnt for firsttime  through

      char * chunk_ptr = buf_ptr+BUFSIZE*(r_pos-CHUNK);
      /* int packet_index=(((uint16_t)chunk_ptr[1]&0xff)<<8)|(chunk_ptr[0]&0xff); */
      /* int last_packet_index=(((uint16_t)chunk_ptr[0x1903de1]&0xff)<<8)|(chunk_ptr[0x1903de0]&0xff); */
      int packet_index=(((uint16_t)chunk_ptr[11]&0xff)<<8)|(chunk_ptr[10]&0xff);
      int last_packet_index=(((uint16_t)chunk_ptr[0x1903deb]&0xff)<<8)|(chunk_ptr[0x1903dea]&0xff);
      
      int index=(r_pos/CHUNK -1);
      //      sem_wait(sem_array[index]);
      while ( status_sem_wait = sem_trywait(sem_array[index]) < 0) { 
	skipped++;
	//	if( !(skipped%1000) ) printf("skipped: %d\n",skipped);
	printf("blocked on sem %d, write to %d instead starting with %x: \n",
	       index, index+1, packet_index);
	index = (index+1)%(BUFNUM/CHUNK);
	//	status_sem_wait = sem_trywait(sem_array[index]);      
	//	continue;
      }
      
      //      printf("locked sem %d, r_pos: %d\n", index, r_pos);
      char outfile_name[256];
      if( index == 0 ) printf("wrote to udp_0, r_pos: %d, first: %x, last: %x, readyet: %d\n",
			      r_pos-CHUNK,packet_index,last_packet_index,is_packetready[index]);
      
      if( is_packetready[index] == 0) {tot_overwritten++; overwritten++;}
      is_packetready[index] = 0;
      
      sprintf(outfile_name,"/mnt/ramdisk/udp_%d.dat", index);//r_pos); 
      //    sprintf(outfile_name,"/mnt/nvme0/udp_%d.dat", r_pos); 
      //      sprintf(outfile_name,"/home/udptester/delete_me/udp_%d.dat", r_pos); 
      int outfile_descriptor = open(outfile_name,  O_CREAT | O_WRONLY | O_DSYNC | O_TRUNC , 00666); //| O_NOATIME
      FILE* outfile_handle = fdopen(outfile_descriptor, "w");
      
      t_start_write[sample_index]=getTime();
      fwrite(buf_ptr+BUFSIZE*(r_pos-CHUNK), BUFSIZE*CHUNK, 1, outfile_handle);
      t_stop_write[sample_index]=getTime();
      tot_written++;
      //      printf("wrote to %s, offset: %u\n", outfile_name, +BUFSIZE*(r_pos-CHUNK));
      
      fclose( outfile_handle );
      close(outfile_descriptor);
      if( !status_sem_wait ) sem_post(sem_array[index]);
      //      printf("unlocked sem %d\n", index);
      
      sample_index++;
      if( sample_index == NSAMPLES ) {
	printStatWritingOnly(tot_written, skipped, tot_overwritten);
       	sample_index=0;
      }
    } // if mod CHUNK

    r_pos=(r_pos+1)%BUFNUM;
    packet_count++;
    // KH B -> b?
    //    total_data_length+=message_len;
    total_data_length+=(message_len*8);
    //printf("pkt_cnt: %d\tsum: %d\n", packet_count, total_data_length);
  }
  
  close(socket_descriptor);
  
  return 0;
}


int bufInit(){
  key_t k = ftok("/dev/shm/file_status",1);
  if (0 > (sharedmem_id = shmget(k, 512*sizeof(int) + A_MAX_SIZE + sizeof(size_t), IPC_CREAT|IPC_EXCL|0666))) {
    printf("shmget error, sharedmem_id = %d, errno: %s\n", sharedmem_id, strerror(errno));
    //    perror(errno);
    return -1;
  }
  is_packetready =(int *)( shmat(sharedmem_id, NULL, 0));  
  for( int i=0; i<(BUFNUM/CHUNK); i++ ) { // = 8196/64
    is_packetready[i] = 1;
  }
  fprintf(stderr,"starting...\n");

  char sem_name[256];
  for( int i=0; i<(BUFNUM/CHUNK); i++ ) { // = 8196/64
    sprintf(sem_name,"/ramdisk-semaphore-%d", i);
    sem_array[i] = sem_open(sem_name, O_CREAT, 00666, 1);//|O_EXCL
  }
  
  //signal(SIGINT,sighandler_int);  
  // signal(SIGQUIT,sighandler_quit);
  uint64_t mem_allocated = (unsigned long)(BUFSIZE*(BUFNUM));
  buf_ptr=malloc(mem_allocated);
  fprintf(stderr,"after malloc\n");
  
  return 0;
}


int createPThreadPriority(pthread_t *thread_id, int scheduler, int priority, void *(*start_routine) (void *), void *arg) {
  pthread_attr_t attr;
  struct sched_param shparam;
  int s;
  
  shparam.sched_priority=priority;       //higher value -> higher priority for SCHED_RR
  s = pthread_attr_init(&attr);
  s = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
  s = pthread_attr_setschedpolicy(&attr, scheduler);
  s = pthread_attr_setschedparam(&attr,&shparam);
  s = pthread_create(thread_id, &attr, start_routine, arg);
  // printf("made thread with id %lu, prio %u\n", *thread_id, priority);
  return s;
}



void printStatOverall() {
  uint64_t t_usec, t_get;
  double t_sec, rate;

  printf("t_start: %llu \n", t_start);
  t_get=getTime();
  printf("t_get:   %llu \n",t_get);
  
  t_usec=t_get-t_start;
  t_sec=t_usec/1000000.0;
  printf("t_usec: %llu \n", t_usec);
  printf("t_sec: %.6f \n", t_sec);
   
  rate=total_data_length;
  rate=rate/t_sec;
  
  printf("received %llu packets, %llu Mb, %.3f Gb/s over %.6f s. %u sequence mismatch events, lost %u packets, max buffers in use: %u\n",
	 (unsigned long long int)packet_count,
	 (unsigned long long int)total_data_length/1024/1024,
	 rate/1024.0/1024.0/1024.0,
	 t_sec,
	 mismatched_packets,
	 lost_packets,
	 (unsigned  int)max_buf_use);
}

void sigHandlerInt(int a) {
  printf("SIGINT\n");
  
  printStatOverall();
  for( int i=0; i<(BUFNUM/CHUNK); i++ ){
    sem_close(sem_array[i]);
    char sem_name[256];
    sprintf(sem_name,"/ramdisk-semaphore-%d", i);
    sem_unlink(sem_name);
  }
  shmdt(is_packetready);
  exit(0);
}

void sigHandlerQuit(int a) {
  printf("SIGQUIT\n");
  for( int i=0; i<(BUFNUM/CHUNK); i++ ){
    sem_close(sem_array[i]);
    char sem_name[256];
    sprintf(sem_name,"/ramdisk-semaphore-%d", i);
    sem_unlink(sem_name);
  }
  shmdt(is_packetready);
  run=0;
}


uint16_t checkPacketSequence(uint16_t prev_packet_index){
  uint8_t *packet;
  uint16_t packet_index;
  
  prev_packet_index++;
  packet=buf_ptr+BUFSIZE*w_pos;
  //    packet_index=(((uint16_t)packet[1])<<8)|packet[0];
  packet_index=(((uint16_t)packet[11])<<8)|packet[10];
  if (is_firstindex==0) {
    is_firstindex=1;
    printf("first idx: %x\n",packet_index);
  } else if (packet_index!=prev_packet_index) { //&& !reset_pointers
    uint16_t n_lost=packet_index-prev_packet_index;
    lost_packets+=n_lost;     
    printf("mismatching sequence %u: %x vs %x. %u packets lost in total\n",++mismatched_packets,packet_index,prev_packet_index,lost_packets);
  }  
  
  
  prev_packet_index=packet_index;
  return prev_packet_index;
}



int main(int argc, char *argv[]) {

  pthread_t thread_rw_id;
  uint16_t prev_packet_index;
  uint32_t buf_use;

  int buf_status = bufInit();
  if (buf_status < 0){
    return 1;
  }
  
  /* key_t k = ftok("/dev/shm/file_status",1); */
  /* if (0 > (sharedmem_id = shmget(k, 512*sizeof(int) + A_MAX_SIZE + sizeof(size_t), IPC_CREAT|IPC_EXCL|0666))) { */
  /*   printf("shmget error, sharedmem_id = %d, errno: %s\n", sharedmem_id, strerror(errno)); */
  /*   //    perror(errno); */
  /*   return 1; */
  /* } */
  /* is_packetready =(int *)( shmat(sharedmem_id, NULL, 0));   */
  /* for( int i=0; i<(BUFNUM/CHUNK); i++ ) { // = 8196/64 */
  /*   is_packetready[i] = 1; */
  /* } */
  /* fprintf(stderr,"starting...\n"); */

  /* char sem_name[256]; */
  /* for( int i=0; i<(BUFNUM/CHUNK); i++ ) { // = 8196/64 */
  /*   sprintf(sem_name,"/ramdisk-semaphore-%d", i); */
  /*   sem_array[i] = sem_open(sem_name, O_CREAT, 00666, 1);//|O_EXCL */
  /* } */
  
  signal(SIGINT,sigHandlerInt);
  signal(SIGQUIT,sigHandlerQuit);
  /* uint64_t mem_allocated = (unsigned long)(BUFSIZE*(BUFNUM)); */
  /* buf_ptr=malloc(mem_allocated); */
  /* fprintf(stderr,"after malloc\n"); */
   
  fprintf(stderr,"after pthread cond_init\n");

  //KH
  createPThreadPriority(&thread_rw_id, 99, 1, &threadWritetoRamdisk, 0);
  //    createPThreadPriority(&thread_rw_id, SCHED_RR, 1, &threadWritetoRamdisk, 0);
  //  createPThreadPriority(&thread_rw_id, SCHED_OTHER, 0, &threadWritetoRamdisk, 0);

  fprintf(stderr,"made pthread\n");
  
  prev_packet_index=0;   
  //int is_firstindex=0;      MM: declare as global 
  while(run) {
    //Allows r_pos to be incremented before checking the packet sequence if r_pos and w_pos are equal.
    if (r_pos==w_pos) {
      while (r_pos==w_pos){
      }
    }
    
    prev_packet_index=checkPacketSequence(prev_packet_index);
    //reset_pointers = 0;
    
    buf_use=(r_pos-w_pos)%BUFNUM;
    if (buf_use>max_buf_use) max_buf_use=buf_use;
    
    w_pos=(w_pos+1)%BUFNUM;
  }
  printf("C\n");
  
  pthread_join(thread_rw_id,0);
  
  printStatOverall();
  free(buf_ptr);
  return 0;
}
