#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <signal.h>
#include <sched.h>

#include <arpa/inet.h>
#include <sys/socket.h>


#include <sys/time.h>
#include <sched.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>



/* #define BUFSIZE (8192+512)   */
/* #define BUFNUM  8192 */
#define BUFSIZE (12296+512)   //bytes
//#define BUFNUM  8192
#define BUFNUM  262144UL
#define CHUNK   2048

uint8_t *buf;

pthread_mutex_t recv_mutex;
pthread_cond_t  recv_cond;

uint32_t run=1;
volatile uint64_t r_pos=0,w_pos=0;

uint64_t data_sum=0,packet_count=0,max_buf_use=0;
unsigned int mismatch=0,lost=0;
uint64_t t_start, t_start_kh, t_stop_kh;

sem_t * sem[BUFNUM/CHUNK]; // 128
int the_sem_no=0;

double gltime() {
 struct timeval tv;
  
 gettimeofday(&tv,0);
 return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}

void sighandler_int(int a) {
 printf("SIGINT\n");
 sem_post(sem[the_sem_no]);
 sem_close(sem[the_sem_no]);
 exit(0);
}


int main(int argc, char *argv[]) {

 
  pthread_t recv_thread_id;
  uint16_t pktidx,pktidx_prev;
  uint8_t *p;
  uint32_t d;

  signal(SIGINT,sighandler_int);

  

  fprintf(stderr,"starting...\n");

  char sname[256];
  void * temp = malloc(BUFSIZE*CHUNK);

  int count=0;
  uint64_t tstart[100], tmid[100], tend[100];
  while(1) { 

    for( int i=1; i<BUFNUM/CHUNK; i++ ) {
      sprintf(sname,"/ramdisk-semaphore-%d", i);
      sem[i] = sem_open(sname, O_RDWR);
      //      printf("waiting on %s\n", sname);
      fflush(stdout);
      sem_wait(sem[i]);
      the_sem_no = i;
      //printf("locked %s\n", sname);
      fflush(stdout);
      
      sprintf(sname, "/mnt/ramdisk/udp_%d.dat", i*1000);
      int fdsrc = open(sname,  O_RDWR |O_SYNC , 00666); //| O_NOATIME
      FILE * fsrc = fdopen(fdsrc, "r");
      //      printf("opened %s\n",sname);
      sprintf(sname, "/mnt/nvme0/udp_%d.dat", i*1000);
      //sprintf(sname, "/home/udptester/delete_me/udp_%d.dat", i*1000);
      int fddest = open(sname,  O_CREAT| O_RDWR | O_SYNC , 00666); //| O_NOATIME
      FILE * fdest = fdopen(fddest, "w");
      //      printf("opened %s\n",sname);

      int idx=count*8+i-1;
      tstart[idx]=gltime();
      fread(temp, 1, BUFSIZE*1000, fsrc);
      tmid[idx]=gltime();
      //      printf("read %d\n",count*8+i-1);
      fwrite(temp, 1, BUFSIZE*1000, fdest);
      tend[idx]=gltime();      
      //      printf("wrote\n");
      fclose(fsrc);
      fclose(fdest);
      int ret = sem_post(sem[i]);
      if ( ret < 0 ) {
	if(errno == EINVAL ) 	printf("sem_post:EINVAL\n");
	if(errno == EOVERFLOW ) 	printf("sem_post:EOVERFLOW\n");	
      }
      //      printf("unlocked %s\n", sname);
      //      printf("-->index: %d\t%lu\t%lu\t%lu\n",idx,tstart[i],tmid[i],tend[i]);
    }
    count++;
    if( count == 10 ) {
      uint64_t avgr=0,avgw=0;
      for(int i=0; i<8*10; i++ ) {
	avgr += tmid[i]-tstart[i];
	avgw += tend[i]-tmid[i];
	//	printf("-> %lu\t%lu\n", tmid[i]-tstart[i], tend[i]-tmid[i]);
	//printf("%lu\t%lu\t%lu\t%f\t%f\n",tstart[i],tmid[i],tend[i],avgr,avgw);
      }
      double totmb=BUFSIZE*1000/(1024.*1024.);
      double rsecs = (avgr)/(1000000*80.);
      double wsecs = (avgw)/(1000000*80.);
      uint64_t tstamp=gltime();
      printf( "size: %.1f MB, time: %f\tread rate: %.3f GB/s, write rate: %.3f GB/s\ttime: %llu\n",
	      totmb,
	      rsecs,
	      totmb/rsecs/1024,
	      totmb/wsecs/1024,
	      tstamp);	      
      fflush(stdout);
      count = 0;  
    }
  }


}
