#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <signal.h>
#include <sched.h>

#include <arpa/inet.h>
#include <sys/socket.h>


#include <sys/time.h>
#include <sched.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>


#define handle_error_en(en, msg) do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error( msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)

/* #define BUFSIZE (8192+512)   */
/* #define BUFNUM  8192 */
#define BUFSIZE (12296+512)   //bytes
#define BUFNUM  8192
//#define BUFNUM  100

uint8_t *buf;

pthread_mutex_t recv_mutex;
pthread_cond_t  recv_cond;

uint32_t run=1;
volatile uint64_t r_pos=0,w_pos=0;

uint64_t data_sum=0,packet_count=0,max_buf_use=0;
unsigned int mismatch=0,lost=0;
#define NSAMPLES 100
uint64_t t_start, t_start_kh[NSAMPLES], t_stop_kh[NSAMPLES];

sem_t * sem[9];

double gltime() {
 struct timeval tv;
  
 gettimeofday(&tv,0);
 return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}


static void * recv_thread(void *data) {
  struct sockaddr_in src_addr,dst_addr;
  int fd,j;
  socklen_t src_len;  


  //  fprintf(stderr,"making socket\n");
  if ((fd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0) {
    perror("socket");
    exit(-1);
  }
  //  fprintf(stderr,"socket fd: %d\n",fd);
  memset((char *) &dst_addr, 0, sizeof(dst_addr));
     
  dst_addr.sin_family = AF_INET;
  dst_addr.sin_port = htons(2345);

  //KH
  dst_addr.sin_addr.s_addr = inet_addr("192.168.1.2");
  //dst_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if( bind(fd , (struct sockaddr*)&dst_addr, sizeof(dst_addr) ) <0 )   {
    perror("bind");
  }  


  unsigned int n,l;
  if (getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &n, &l) == -1) {
   printf("setsockopt failed\n");
  } else printf("SO_RCVBUF: %d\n",n);

  n = 1024 * 1024*512;
  if (setsockopt(fd, SOL_SOCKET, SO_RCVBUFFORCE, &n, sizeof(n)) == -1) {
   printf("setsockopt failed\n");
  }

  if (getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &n, &l) == -1) {
   printf("setsockopt failed\n");
  }  else printf("SO_RCVBUF: %d\n",n);

  t_start=gltime();
   
  int count=0,skipped=0,fidx=0;
  while(run) {
    src_len=sizeof(src_addr);
    //    fprintf(stderr,"calling recvfrom ..., r_pos=%d, ptr: %lu\n",r_pos,buf+BUFSIZE*r_pos);
    if ((j=recvfrom(fd, buf+BUFSIZE*r_pos, BUFSIZE, 0, (struct sockaddr *) &src_addr, &src_len))<0) {
    perror("recvfrom");
    }


    // KH write pkt to disk
    fidx=r_pos;
    if( !(r_pos%1000) && r_pos>0) { 
      int index=(r_pos/1000);
      //      sem_wait(sem[index]);
      int ret = sem_trywait(sem[index]);
      //      if( ret == -1 && errno == EAGAIN ){
      if( ret == -1 ){
      skipped++;
	//	if( !(skipped%1000) ) printf("skipped: %d\n",skipped);
      //      printf("blocked on sem %d\n", index);
	fidx += 1000; if (fidx == 9000 ) fidx = 1000;
	//	continue;
      }
      //      printf("locked sem %d, r_pos: %d\n", index, r_pos);
      char fname[256];
      sprintf(fname,"/mnt/ramdisk/udp_%d.dat", fidx);//r_pos); 
      //    sprintf(fname,"/mnt/nvme0/udp_%d.dat", r_pos); 
      //      sprintf(fname,"/home/udptester/delete_me/udp_%d.dat", r_pos); 
      int fdd = open(fname,  O_CREAT | O_WRONLY | O_SYNC , 00666); //| O_NOATIME
      FILE* file_handle = fdopen(fdd, "wb");


      
      t_start_kh[count]=gltime();
      //fwrite(buf+BUFSIZE*r_pos, BUFSIZE, 1, file_handle);
      fwrite(buf+BUFSIZE*(r_pos-1000), BUFSIZE*1000, 1, file_handle);
      t_stop_kh[count]=gltime();
     
      fclose( file_handle );
      close(fdd);
      if( !ret ) sem_post(sem[index]);
      //      printf("unlocked sem %d\n", index);

      count++;
      if( count == NSAMPLES ) {
	count=0;
	uint64_t diff=0;
	for( int i=0; i<NSAMPLES; i++ ) {
	  diff += t_stop_kh[i] - t_start_kh[i];
	}
	double avgdiff = (double)diff/NSAMPLES;
	double fracamt = (double)BUFSIZE*1000;///(1024*1024*1024); // GB
	double fractime = (avgdiff)/1000000;
	uint64_t tstamp = gltime();
	printf("Wrote %.3f MB in %.7f s, rate (GB/s): %f\ttime: %llu\tskipped: %d\n",
	       fracamt/1024/2014,
	       fractime,
	       fracamt/(1024*1024*1024*fractime),
	       tstamp,
	       skipped);  
	fflush(stdout);
      }
    
      
    }

    r_pos=(r_pos+1)%BUFNUM;
    packet_count++;
    // KH B -> b?
    //    data_sum+=j;
    data_sum+=(j*8);
    //printf("pkt_cnt: %d\tsum: %d\n", packet_count, data_sum);
    if ((r_pos&15)==0)    pthread_cond_signal(&recv_cond);
  }
  
  close(fd);



  
  pthread_cond_signal(&recv_cond);
  return 0;
}



int prio_pthread_create(pthread_t *thread_id, int scheduler, int priority, void *(*start_routine) (void *), void *arg) {
 pthread_attr_t attr;
 struct sched_param shparam;
 int s;
 
 shparam.sched_priority=priority;       //higher value -> higher priority for SCHED_RR
 s = pthread_attr_init(&attr);
 s = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
 s = pthread_attr_setschedpolicy(&attr, scheduler);
 s = pthread_attr_setschedparam(&attr,&shparam);
 s = pthread_create(thread_id, &attr, start_routine, arg);
 // printf("made thread with id %lu, prio %u\n", *thread_id, priority);
 return s;
}



void printstat() {
 uint64_t t;
 double T,R;
 
 t=gltime()-t_start;
 T=t/1000000.0;
 
 R=data_sum;
 R=R/T;
 
 printf("received %llu packets, %llu Mb, %.3f Gb/s over %.2f s. %u sequence mismatch events, lost %u packets, max buffers in use: %u\n",(unsigned long long int)packet_count,(unsigned long long int)data_sum/1024/1024,R/1024.0/1024.0/1024.0,T,mismatch,lost,(unsigned  int)max_buf_use);
}

void sighandler_int(int a) {
 printf("SIGINT\n");

 printstat();
 for( int i=0; i<9; i++ ){
   sem_close(sem[i]);
   char sname[256];
   sprintf(sname,"/ramdisk-semaphore-%d", i);
   sem_unlink(sname);
 }
   exit(0);
}

void sighandler_quit(int a) {
 printf("SIGQUIT\n");
 for( int i=0; i<9; i++ ){
   sem_close(sem[i]);
   char sname[256];
   sprintf(sname,"/ramdisk-semaphore-%d", i);
   sem_unlink(sname);
 }
 run=0;
}



int main(int argc, char *argv[]) {

  pthread_t recv_thread_id;
  uint16_t pktidx,pktidx_prev;
  uint8_t *p;
  uint32_t d;


  fprintf(stderr,"starting...\n");

  char sname[256];
  for( int i=0; i<9; i++ ) {
    sprintf(sname,"/ramdisk-semaphore-%d", i);
    sem[i] = sem_open(sname, O_CREAT, 00666, 1);//|O_EXCL
  }
  
  signal(SIGINT,sighandler_int);  
  signal(SIGQUIT,sighandler_quit);
 
  buf=malloc(BUFSIZE*(BUFNUM));
  fprintf(stderr,"after malloc\n");
   
  pthread_cond_init(&recv_cond, 0);
  fprintf(stderr,"after pthread cond_init\n");

  //KH
  prio_pthread_create(&recv_thread_id, 99, 1, &recv_thread, 0);
  //    prio_pthread_create(&recv_thread_id, SCHED_RR, 1, &recv_thread, 0);
  //  prio_pthread_create(&recv_thread_id, SCHED_OTHER, 0, &recv_thread, 0);

  fprintf(stderr,"made pthread\n");
  
  pktidx_prev=0;   
  int pc=0;  
  while(run) {
    if (r_pos==w_pos) {
      pthread_mutex_lock(&recv_mutex);
      //printf("mutex lock\n");
      while (r_pos==w_pos) pthread_cond_wait(&recv_cond, &recv_mutex);
      pthread_mutex_unlock(&recv_mutex);
      //      printf("mutex unlock\n");
    }

    pktidx_prev++;
    p=buf+BUFSIZE*w_pos;
    pktidx=(((uint16_t)p[1])<<8)|p[0];
    
    if (pc==0) {
      pc=1;
      printf("first idx: %u\n",pktidx);
    } else if (pktidx!=pktidx_prev) {
      uint16_t a=pktidx-pktidx_prev;
      lost+=a;     
      printf("mismatching sequence %u: %u vs %u. %u packets lost in total\n",++mismatch,pktidx,pktidx_prev,lost);
    }  
    
    
    d=(r_pos-w_pos)%BUFNUM;
    if (d>max_buf_use) max_buf_use=d;

   
    pktidx_prev=pktidx;

    w_pos=(w_pos+1)%BUFNUM;
  }  
  printf("C\n");

  pthread_join(recv_thread_id,0);

  printstat();
  free(buf);
  return 0;
}
