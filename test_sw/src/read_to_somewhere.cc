#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <signal.h>
#include <sched.h>

#include <arpa/inet.h>
#include <sys/socket.h>


#include <sys/time.h>
#include <sched.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#ifdef XRD
#include <cppunit/extensions/HelperMacros.h>
#include "XrdCl/XrdClFile.hh"
#include "CppUnitXrdHelpers.hh"
using namespace XrdCl;
#endif

/* #define BUFSIZE (8192+512)   */
/* #define BUFNUM  8192 */
#define BUFSIZE (12296+512)   //bytes
//#define BUFNUM  8192

/* #define BUFNUM  262144UL */
/* #define CHUNK   2048 */

#define BUFNUM  1048576UL
#define CHUNK   2048

#define XRDCHUNKS 16

uint8_t *buf;

pthread_mutex_t recv_mutex;
pthread_cond_t  recv_cond;

uint32_t run=1;
volatile uint64_t r_pos=0,w_pos=0;

uint64_t data_sum=0,packet_count=0,max_buf_use=0;
unsigned int mismatch=0,lost=0;
uint64_t t_start, t_start_kh, t_stop_kh;

sem_t * sem[BUFNUM/CHUNK]; // 128
int the_sem_no=0;
int shm_id=0;
/* Assume a cannot point towards an area larger than 4096 bytes. */
#define A_MAX_SIZE (size_t)4096
int * statptr;

double gltime() {
  struct timeval tv;
  
  gettimeofday(&tv,0);
  return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}

void sighandler_int(int a) {
  printf("SIGINT\n");
  sem_post(sem[the_sem_no]);
  sem_close(sem[the_sem_no]);
  shmdt(statptr);
  exit(0);
}


int main(int argc, char *argv[]) {

  key_t k = ftok("/dev/shm/file_status",1);
  if (0 > (shm_id = shmget(k, 512*sizeof(int) + A_MAX_SIZE + sizeof(size_t), 0666))) {
    printf("shmget error, shm_id = %d, errno: %s\n", shm_id, strerror(errno));
    //    perror(errno);
    return 1;
  }
  statptr =(int *)(shmat(shm_id, NULL, 0));  

  
  pthread_t recv_thread_id;
  uint16_t pktidx,pktidx_prev;
  uint8_t *p;
  uint32_t d;

  struct stat stat_arr[BUFNUM/CHUNK];

  
  signal(SIGINT,sighandler_int);

  

  fprintf(stderr,"starting...\n");

  char sname[256];
  char * temp = (char *)malloc(BUFSIZE*CHUNK);

  int count=0, first_time=1, mismatch=0,wrote=0;;
  unsigned last_counter;
  uint64_t tstart[BUFNUM/CHUNK], tmid[BUFNUM/CHUNK], tend[BUFNUM/CHUNK];
  uint64_t t_overhead_start[BUFNUM/CHUNK], t_overhead_end[BUFNUM/CHUNK];
  struct stat filestat;


  for( int i=0; i<(BUFNUM/CHUNK); i++ ) { 
    stat_arr[i].st_mtim.tv_sec = 0;

    sprintf(sname,"/ramdisk-semaphore-%d", i);
    sem[i] = sem_open(sname, O_RDWR);
    if( stat(sname, NULL) == ENOENT ) {
      printf("%s doesn't exist, skipping\n");
      continue;
    }
    //    stat_arr[i].st_mtim.tv_ns = 0;    
   } 


      //sprintf(sname, "/home/udptester/delete_me/udp_%d.dat", i*1000);
      //      printf("opened %s\n",sname);
      //      int fddest = open(sname,  O_CREAT| O_RDWR | O_SYNC , 00666); //| O_NOATIME//| 

      //      printf("opened %s\n",sname);

      //      int idx=count*8+i-1;



      // check for overwrites :
      //1903df0 ff73 ff73 0000 0000 0000 0000 0000 0000#ifdef XRD
#ifdef XRD
  File f1;
  std::string address = "root://eosuser.cern.ch:1094/";
  std::string dataPath = "/eos/user/k/khahn/test";
#ifdef IOVEC
  int iovcnt = 2;
  iovec iov[iovcnt];
  iov[0].iov_base = temp;
  iov[0].iov_len  = BUFSIZE*CHUNK/2;
  iov[1].iov_base = temp+(CHUNK/2);
  iov[1].iov_len  = BUFSIZE*CHUNK/2;
#endif

#ifdef CHUNKVEC
  XrdCl::ChunkList chunks;
#endif
#endif

#ifdef FUSE
  int fddest [BUFNUM/CHUNK];  
  FILE * fdest [BUFNUM/CHUNK];
  for( int i=0; i<(BUFNUM/CHUNK); i++ ) {
      sprintf(sname, "/eos/test/udp_%d.dat", i);
      //      sprintf(sname, "/home/udptester/eos/test/udp_%d.dat", i);
      fddest[i] = open(sname,  O_CREAT| O_WRONLY  , 00666); //| O_NOATIME//| | O_SYNC
      fdest[i] = fdopen(fddest[i], "w");
  }
#endif      

  
  while(1) { 

    for( int i=0; i<(BUFNUM/CHUNK); i++ ) {
      t_overhead_start[i] = gltime();
      sem_wait(sem[i]);
      the_sem_no = i; // for sighandle
     //      printf("locked %s\n", sname);
      
      sprintf(sname, "/mnt/ramdisk/udp_%d.dat", i);

      int fdsrc = open(sname,  O_RDONLY |O_SYNC , 00666); //| O_NOATIME
      if( fdsrc < 0 ) {
	//printf( "problem opening %s\n", sname);
	//close(fdsrc);
	sem_post(sem[i]); //check return ...

	continue;
      }
      fstat(fdsrc, &filestat);
      if( filestat.st_mtim.tv_sec <= stat_arr[i].st_mtim.tv_sec ) { // also ns?
	//printf("%s not updated, skipping ...\n",sname );
	close(fdsrc);
	sem_post(sem[i]); //check return ...
	continue;
      }
      stat_arr[i]  = filestat;
      
      FILE * fsrc = fdopen(fdsrc, "r");
      //printf("opened %s\n",sname);

      // ---> read
      tstart[i]=gltime();
      fread(temp, sizeof(char), BUFSIZE*CHUNK, fsrc);
      tmid[i]=gltime();


      // ---> write
#ifndef EOS
      sprintf(sname, "/mnt/nvme0/udp_%d.dat", i);
#else
// #ifdef FUSE
//       sprintf(sname, "/eos/test/udp_%d.dat", i);
//       //      sprintf(sname, "/home/udptester/eos/test/udp_%d.dat", i);
// #endif      
#endif


#ifndef XRD    
      // int fddest = open(sname,  O_CREAT| O_WRONLY  , 00666); //| O_NOATIME//| | O_SYNC
      // FILE * fdest = fdopen(fddest, "w");
      //      fwrite(temp, 1, BUFSIZE*CHUNK, fdest);
      fwrite(temp, 1, BUFSIZE*CHUNK, fdest[i]);
      tend[i]=gltime();
#else
      sprintf(sname, "/udp_%d.dat",i);
      std::string filePath = dataPath + sname;
      std::string fileUrl = address;
      fileUrl += filePath;
      CPPUNIT_ASSERT( f1.Open( fileUrl, OpenFlags::Delete | OpenFlags::Update,
			     Access::UR | Access::UW ).IsOK() );

#ifdef IOVEC
    CPPUNIT_ASSERT( f1.WriteV( 0, iov, iovcnt ).IsOK() );
#elif CHUNKVEC
    for( int j=0; j<XRDCHUNKS; j++ ) {
      chunks.push_back( XrdCl::ChunkInfo( j*(BUFSIZE*CHUNK)/XRDCHUNKS, (BUFSIZE*CHUNK)/XRDCHUNKS, temp ) );
    }
    CPPUNIT_ASSERT_XRDST( f1.VectorWrite( chunks ) );
    chunks.clear();
#else
    CPPUNIT_ASSERT( f1.Write( 0, BUFSIZE*CHUNK, temp ).IsOK() );
#endif
    //  cout << "wrote 4MB ... " << endl;
    CPPUNIT_ASSERT( f1.Sync().IsOK() );
    tend[i]=gltime();
#endif      

    // ---> check for overwrites
      char * cptr = temp;
      unsigned first_counter = ((0xff&cptr[11]) << 8UL) | (0xff&cptr[10]) ;
      if( !first_time ) { 
	/* printf("%d :: first: 0x%x\tlast: 0x%x\tc0: %x\tc1: %x\n", */
	/*        i, */
	/*        first_counter, */
	/*        last_counter, */
	/*        0xff&cptr[0], */
	/*        0xff&cptr[1]); */
	if( first_counter != last_counter+1 ) {
	  printf("mismatch (%d) :: first: 0x%x\tprev last: 0x%x\n",
		 i,
		 first_counter,
		 last_counter );
	  mismatch++;
	}
      } else {
	first_time = 0;
	/* for( int i=0; i<BUFSIZE*CHUNK; i++ ) { */
	/*   if( !(i%2) && i>0 &&(i%16)) printf("_"); */
	/*   if( !(i%16) && i>0) printf("\n"); */
	/*   if( !(i%16) ) printf("%8x\t", i ); */
	/*   printf("%2x",(0xff&temp[i])); */
	/* } */
      }                       

      if( i < 493 ) {
	//	last_counter = ((0xff&cptr[0x1903de1]) << 8UL) | (0xff&cptr[0x1903de0]);
	last_counter = ((0xff&cptr[0x1903e1b]) << 8UL) | (0xff&cptr[0x1903e1a]);
      }
      else { 
	last_counter = ((0xff&cptr[0x144ffe1]) << 8UL) | (0xff&cptr[0x144ffe0]);	
	printf ("last counter for pkt %d :  %x\n", i, last_counter );
      }
	//      printf("last: 0x%x\tc0: 0x%x\tc1: 0x%x\n", last_counter, 0xff&cptr[0x1903de0], 0xff&cptr[0x1903de1]);
      
      //      printf("read %d\n",BUFSIZE*CHUNK);


      //      printf("wrote to %s\n", sname);
      fflush(stdout);
      close(fdsrc);
      fclose(fsrc);
#ifndef XRD
      // close(fddest);
      // fclose(fdest);
#else
      CPPUNIT_ASSERT( f1.Close().IsOK() );
#endif
      wrote++;
      count++;
      statptr[i] = 1 ; // mark as read
      int ret = sem_post(sem[i]);
      if ( ret < 0 ) {
	printf("error: sem_post\n");
	if(errno == EINVAL ) 	printf("sem_post:EINVAL\n");
	if(errno == EOVERFLOW ) 	printf("sem_post:EOVERFLOW\n");	
      }
      //      printf("unlocked %s\n", sname);
      //      printf("-->index: %d\t%lu\t%lu\t%lu\n",idx,tstart[i],tmid[i],tend[i]);
      t_overhead_end[i] = gltime();
      if( !(count%100) ) { 
	uint64_t avgr=0,avgw=0,avgwOH=0;
	for(int j=0; j<100; j++ ) {
	  avgr += (tmid[j]-tstart[j]);
	  avgw += (tend[j]-tmid[j]);
	  avgwOH += (t_overhead_end[j]-t_overhead_start[j]);	  
	  //	  printf( "j: %d\tend: %lu\tstart: %lu\n", j , t_overhead_end[j], t_overhead_start[j]);	  
	  //      printf("%lu\t%lu\t%lu\t%lu\t%lu\n",tstart[i],tmid[i],tend[i],avgr,avgw);
	}
	/* double totmb=(BUFSIZE*CHUNK)/(1024.*1024.); */
	/* double rsecs = (avgr)/(1000000.*(BUFNUM/CHUNK)); */
	/* double wsecs = (avgw)/(1000000.*(BUFNUM/CHUNK)); */
	double totmb=(BUFSIZE*CHUNK)/(1024.*1024.);
	double rsecs = (avgr)/(1000000.*100);
	double wsecs = (avgw)/(1000000.*100);
	double wOHsecs = (avgwOH)/(1000000.*100);	
	uint64_t tstamp=gltime();
        double totgb = (double)wrote*(totmb/1024.);
	printf( "size: %.1f MB, time: %f\tread rate: %.3f GB/s, write rate: %.3f GB/s\ttime: %llu\twpkt: %d (%f GB)\tmismatch: %d rateOH: %f\n",
		totmb,
		rsecs,
		totmb/rsecs/1024,
		totmb/wsecs/1024,
		tstamp,
		wrote,
                totgb,
		mismatch,
		totmb/wOHsecs/1024);	      
	fflush(stdout);
	count = 0;  
      }

    } //chunk loop
  } //run
}
