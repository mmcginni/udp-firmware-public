#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <signal.h>
#include <sched.h>
#include <unistd.h>
#include <math.h>
 
#define BUFSIZE 8192+64

int run=1;
int dump=0;
uint64_t data_sum=0,packet_count=0;
unsigned int mismatch=0,lost=0;
uint64_t t_start;


double gltime() {
 struct timeval tv;
  
 gettimeofday(&tv,0);
 return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}



void sighandler_int(int a) {

 
 exit(0);
}

void sighandler_quit(int a) {
 printf("SIGQUIT\n");
 dump=1;
}

 
int main(int argc, char ** argv) {

  struct sockaddr_in src_addr,dst_addr;
  struct sockaddr_in dst_addr2;
  int fd;
  int  j;
  unsigned src_len;
  uint8_t buf[BUFSIZE];


  signal(SIGINT,sighandler_int);
  signal(SIGQUIT,sighandler_quit);

  struct sched_param param;
  param.sched_priority = 99;
  if (sched_setscheduler(0, SCHED_FIFO, & param) != 0) {
      perror("sched_setscheduler");
      exit(EXIT_FAILURE);  
  }

  if ((fd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0) {
    perror("socket");
    exit(-1);
  }

  memset((char *) &dst_addr, 0, sizeof(dst_addr));
     
  dst_addr.sin_family = AF_INET;
  dst_addr.sin_port = htons(1234);
  dst_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  dst_addr2.sin_family = AF_INET;
  dst_addr2.sin_port = htons(1234);
  dst_addr2.sin_addr.s_addr = inet_addr("192.168.1.2");; 
  
     
  if( bind(fd , (struct sockaddr*)&dst_addr, sizeof(dst_addr) ) <0 )   {
    perror("bind");
  }  
  
 
   
  while(run) {
  
    t_start=gltime();
    sendto(fd,buf,80,0,(struct sockaddr *)&dst_addr2,sizeof(dst_addr2));
  
    src_len=sizeof(src_addr);
    if ((j=recvfrom(fd, buf, BUFSIZE, 0, (struct sockaddr *) &src_addr, &src_len))<0) {
      perror("recvfrom");
    }
    printf("%ld\n",lround(gltime()-t_start));
 //   usleep(200);
  }

  close(fd);
  return 0;
}
