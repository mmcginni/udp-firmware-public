#!/bin/bash

grep Wrote udp_with_disk.log | awk '{print $9":"$11}' > udp_with_disk.dat
grep Wrote udp_with_nvme.log | awk '{print $9":"$11}' > udp_with_nvme.dat
grep size nvme_write.log | awk '{print $8":"$12":"$15}' > nvme_write.dat
grep size disk_write.log | awk '{print $8":"$12":"$15}' > disk_write.dat
