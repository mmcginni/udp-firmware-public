#!/bin/bash

ifconfig enp1s0f0 192.168.1.2 up mtu 9000

echo 1000000 >  /proc/sys/net/core/netdev_max_backlog 
echo 1000000000 >   /proc/sys/net/core/rmem_max
echo 0 >  /proc/sys/net/ipv4/ip_forward

sysctl -w net.core.rmem_default=838860800
sysctl -w net.core.rmem_max=1677721600



echo "5579370  7439170  11158740" > /proc/sys/net/ipv4/udp_mem


echo 100000000 >  /proc/sys/net/ipv4/udp_rmem_min

ethtool -G enp1s0f0 rx 4096
##ethtool -K enp1s0f0 rx-gro-hw on
ethtool -K enp1s0f0 rx on
ethtool -K  enp1s0f0 rx-all on

iptables -F  
iptables -X  

iptables -F -t nat 
iptables -X -t nat 

arp -s 192.168.1.82 aa:bb:cc:dd:ef:52


swapoff -a

gcc -W -Wall -ggdb -Wno-unused-parameter -o udp_recv2 udp_recv2.c  -lpthread
gcc -W -Wall -ggdb -Wno-unused-parameter -o udp_latency udp_latency.c -lm


#netstat -s |grep -A6 Udp:
#ethtool -S enp1s0f0 |grep -i rx.*error
