#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <signal.h>
#include <sched.h>

#include <arpa/inet.h>
#include <sys/socket.h>


#include <sys/time.h>
#include <sched.h>
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>

#include <sys/ipc.h>
#include <sys/shm.h>

/* Assume a cannot point towards an area larger than 4096 bytes. */
#define A_MAX_SIZE (size_t)4096
int * statptr;

#define handle_error_en(en, msg) do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error( msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)

/* #define BUFSIZE (8192+512)   */
/* #define BUFNUM  8192 */
#define BUFSIZE (12296+512)   //bytes
/* #define BUFNUM  65536 */
/* #define CHUNK   512 */
//#define BUFNUM  262144UL

/* #define BUFNUM  262144UL */
/* #define CHUNK   2048 */

#define BUFNUM  1048576UL
#define CHUNK   2048

uint8_t *buf;

pthread_mutex_t recv_mutex;
pthread_cond_t  recv_cond;

uint32_t run=1;
volatile uint64_t r_pos=0,w_pos=0;

uint64_t data_sum=0,packet_count=0,max_buf_use=0;
unsigned int mismatch=0,lost=0;
#define NSAMPLES 100
uint64_t t_start, t_start_kh[NSAMPLES], t_stop_kh[NSAMPLES];
uint64_t t_recv[NSAMPLES];

sem_t * sem[BUFNUM/CHUNK];
  int shm_id = 0;

int reset_pointers = 0;

double gltime() {
 struct timeval tv;
  
 gettimeofday(&tv,0);
 return (uint64_t)(tv.tv_sec)*1000000+(uint64_t)tv.tv_usec;   
}


static void * recv_thread(void *data) {
  struct sockaddr_in src_addr,dst_addr;
  int fd,j;
  socklen_t src_len;  


  FILE * cache_file = fopen("/proc/sys/vm/drop_caches", "wr");
  
  //  fprintf(stderr,"making socket\n");
  if ((fd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0) {
    perror("socket");
    exit(-1);
  }
  //  fprintf(stderr,"socket fd: %d\n",fd);
  memset((char *) &dst_addr, 0, sizeof(dst_addr));
     
  dst_addr.sin_family = AF_INET;
  dst_addr.sin_port = htons(2345);

  //KH
  dst_addr.sin_addr.s_addr = inet_addr("192.168.1.2");
  //dst_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if( bind(fd , (struct sockaddr*)&dst_addr, sizeof(dst_addr) ) <0 )   {
    perror("bind");
  }  


  unsigned int n,l;
  if (getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &n, &l) == -1) {
   printf("setsockopt failed\n");
  } else printf("SO_RCVBUF: %d\n",n);

  n = 1024 * 1024*512;
  if (setsockopt(fd, SOL_SOCKET, SO_RCVBUFFORCE, &n, sizeof(n)) == -1) {
   printf("setsockopt failed\n");
  }

  if (getsockopt(fd, SOL_SOCKET, SO_RCVBUF, &n, &l) == -1) {
   printf("setsockopt failed\n");
  }  else printf("SO_RCVBUF: %d\n",n);

   
  int count=0,skipped=0,fidx=0;
  int first_time=1;
  int tot_overwritten=0, overwritten=0;
  int total_written=0;
  while(run) {
    src_len=sizeof(src_addr);
    //    fprintf(stderr,"calling recvfrom ..., r_pos=%d, ptr: %lu\n",r_pos,buf+BUFSIZE*r_pos);
    t_start=gltime();
    //    if ((j=recvfrom(fd, buf+BUFSIZE*r_pos, BUFSIZE, 0, (struct sockaddr *) &src_addr, &src_len))<0) {
    int waited_period=0;
    while ((j=recvfrom(fd, buf+BUFSIZE*r_pos, BUFSIZE, MSG_DONTWAIT, (struct sockaddr *) &src_addr, &src_len))<0) {
      uint64_t t_wait = gltime();
      if( (double)(t_wait - t_start)/1000000 > 5. ) {
	printf("waited 5sec for data ... segments overwritten in prev cycle: %d\n",
	       overwritten );
	overwritten=0;
	fflush(stdout);
	if( !waited_period && packet_count  )  {
	  // flush the last packets to udp_0 ...
	  int index=(r_pos/CHUNK); // no -1
	  char fname[256];
	  sem_wait(sem[index]);      
	  sprintf(fname,"/mnt/ramdisk/udp_%d.dat", index);//r_pos); 
	  printf("flushing last packets to %s\n", fname);
	  int fdd = open(fname,  O_CREAT | O_WRONLY | O_DSYNC | O_TRUNC , 00666); //| O_NOATIME
	  FILE* file_handle = fdopen(fdd, "w");
	  fwrite(buf+BUFSIZE*(r_pos-(r_pos%CHUNK)), BUFSIZE*(r_pos%CHUNK), 1, file_handle);
	  fclose( file_handle );
	  close(fdd);
	  sem_post(sem[index]);
	  pthread_cond_signal(&recv_cond);
	} else {
	  // do this 5s later, igves time for cond above and check catchup
	  r_pos = 0; 
	  w_pos = 0; 
	}
	waited_period++;

	reset_pointers = 1;
	t_start = t_wait;
      }
      continue;
      //    perror("recvfrom");
    }

    t_recv[count]=gltime()-t_start;
    //printf("first idx: %x last idx: %x\n",pktidx,lpktidx);

    // KH write pkt to disk
    fidx=r_pos; int ret;
    if( !(r_pos%CHUNK) && r_pos) { // pktcnt for firsttime  through

      char * ptr = buf+BUFSIZE*(r_pos-CHUNK);
      /* int pktidx=(((uint16_t)ptr[1]&0xff)<<8)|(ptr[0]&0xff); */
      /* int lpktidx=(((uint16_t)ptr[0x1903de1]&0xff)<<8)|(ptr[0x1903de0]&0xff); */
      int pktidx=(((uint16_t)ptr[11]&0xff)<<8)|(ptr[10]&0xff);
      int lpktidx=(((uint16_t)ptr[0x1903deb]&0xff)<<8)|(ptr[0x1903dea]&0xff);
      
      int index=(r_pos/CHUNK -1);
      //      sem_wait(sem[index]);
      while ( ret = sem_trywait(sem[index]) < 0) { 
	skipped++;
	//	if( !(skipped%1000) ) printf("skipped: %d\n",skipped);
	printf("blocked on sem %d, write to %d instead starting with %x: \n",
	       index, index+1, pktidx);
	index = (index+1)%(BUFNUM/CHUNK);
	//	ret = sem_trywait(sem[index]);      
	//	continue;
      }
      
	//      printf("locked sem %d, r_pos: %d\n", index, r_pos);
      char fname[256];
      if( index == 0 ) printf("wrote to udp_0, r_pos: %d, first: %x, last: %x, readyet: %d\n",
			      r_pos-CHUNK,pktidx,lpktidx,statptr[index]);

      if( statptr[index] == 0) {tot_overwritten++; overwritten++;}
      statptr[index] = 0;
      
      sprintf(fname,"/mnt/ramdisk/udp_%d.dat", index);//r_pos); 
      //    sprintf(fname,"/mnt/nvme0/udp_%d.dat", r_pos); 
      //      sprintf(fname,"/home/udptester/delete_me/udp_%d.dat", r_pos); 
      int fdd = open(fname,  O_CREAT | O_WRONLY | O_DSYNC | O_TRUNC , 00666); //| O_NOATIME
      FILE* file_handle = fdopen(fdd, "w");
      
      t_start_kh[count]=gltime();
      fwrite(buf+BUFSIZE*(r_pos-CHUNK), BUFSIZE*CHUNK, 1, file_handle);
      t_stop_kh[count]=gltime();
      total_written++;
      //      printf("wrote to %s, offset: %u\n", fname, +BUFSIZE*(r_pos-CHUNK));
     
      fclose( file_handle );
      close(fdd);
      if( !ret ) sem_post(sem[index]);
      //      printf("unlocked sem %d\n", index);
      
      count++;
      if( count == NSAMPLES ) {
	count=0;
	uint64_t diff=0,recv=0;
	for( int i=0; i<NSAMPLES; i++ ) {
	  diff += t_stop_kh[i] - t_start_kh[i];
	  recv += t_recv[i];
	}
	double avgdiff = (double)diff/NSAMPLES;
	double avgrecv = (double)recv/NSAMPLES;
	double fracamt = (double)BUFSIZE*CHUNK;///(1024*1024*1024); // GB
	double fractime = (avgdiff)/1000000;
	uint64_t tstamp = gltime();
	double totgb = (double)(total_written)*BUFSIZE*CHUNK/(1024.*1024.*1024.);
	printf("Wrote %.3f MB in %.7f s, rate (GB/s): %f\ttime: %llu\tskipped: %d, ovrwrt/tot: %d/%d (%.2f GB)\n",
	       fracamt/1024/1024,
	       fractime,
	       fracamt/(1024*1024*1024*fractime),
	       tstamp,
	       skipped,
	       tot_overwritten,
	       total_written,
	       totgb);  
	fflush(stdout);
      }
    } // if mod CHUNK

    r_pos=(r_pos+1)%BUFNUM;
    packet_count++;
    // KH B -> b?
    //    data_sum+=j;
    data_sum+=(j*8);
    //printf("pkt_cnt: %d\tsum: %d\n", packet_count, data_sum);
    if ((r_pos&15)==0)    pthread_cond_signal(&recv_cond);
  }
  
  close(fd);



  
  pthread_cond_signal(&recv_cond);
  return 0;
}



int prio_pthread_create(pthread_t *thread_id, int scheduler, int priority, void *(*start_routine) (void *), void *arg) {
 pthread_attr_t attr;
 struct sched_param shparam;
 int s;
 
 shparam.sched_priority=priority;       //higher value -> higher priority for SCHED_RR
 s = pthread_attr_init(&attr);
 s = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
 s = pthread_attr_setschedpolicy(&attr, scheduler);
 s = pthread_attr_setschedparam(&attr,&shparam);
 s = pthread_create(thread_id, &attr, start_routine, arg);
 // printf("made thread with id %lu, prio %u\n", *thread_id, priority);
 return s;
}



void printstat() {
 uint64_t t;
 double T,R;
 
 t=gltime()-t_start;
 T=t/1000000.0;
 
 R=data_sum;
 R=R/T;
 
 printf("received %llu packets, %llu Mb, %.3f Gb/s over %.2f s. %u sequence mismatch events, lost %u packets, max buffers in use: %u\n",(unsigned long long int)packet_count,(unsigned long long int)data_sum/1024/1024,R/1024.0/1024.0/1024.0,T,mismatch,lost,(unsigned  int)max_buf_use);
}

void sighandler_int(int a) {
 printf("SIGINT\n");

 printstat();
 for( int i=0; i<(BUFNUM/CHUNK); i++ ){
   sem_close(sem[i]);
   char sname[256];
   sprintf(sname,"/ramdisk-semaphore-%d", i);
   sem_unlink(sname);
 }
 shmdt(statptr);
 exit(0);
}

void sighandler_quit(int a) {
 printf("SIGQUIT\n");
 for( int i=0; i<(BUFNUM/CHUNK); i++ ){
   sem_close(sem[i]);
   char sname[256];
   sprintf(sname,"/ramdisk-semaphore-%d", i);
   sem_unlink(sname);
 }
 shmdt(statptr);
 run=0;
}



int main(int argc, char *argv[]) {

  pthread_t recv_thread_id;
  uint16_t pktidx,pktidx_prev;
  uint8_t *p;
  uint32_t d;

  key_t k = ftok("/dev/shm/file_status",1);
  if (0 > (shm_id = shmget(k, 512*sizeof(int) + A_MAX_SIZE + sizeof(size_t), IPC_CREAT|IPC_EXCL|0666))) {
    printf("shmget error, shm_id = %d, errno: %s\n", shm_id, strerror(errno));
    //    perror(errno);
    return 1;
  }
  statptr =(int *)( shmat(shm_id, NULL, 0));  
  for( int i=0; i<(BUFNUM/CHUNK); i++ ) { // = 8196/64
    statptr[i] = 1;
  }
  fprintf(stderr,"starting...\n");

  char sname[256];
  for( int i=0; i<(BUFNUM/CHUNK); i++ ) { // = 8196/64
    sprintf(sname,"/ramdisk-semaphore-%d", i);
    sem[i] = sem_open(sname, O_CREAT, 00666, 1);//|O_EXCL
  }
  
  signal(SIGINT,sighandler_int);  
  signal(SIGQUIT,sighandler_quit);
  uint64_t khsize = (unsigned long)(BUFSIZE*(BUFNUM));
  buf=malloc(khsize);
  fprintf(stderr,"after malloc\n");
   
  pthread_cond_init(&recv_cond, 0);
  fprintf(stderr,"after pthread cond_init\n");

  //KH
  prio_pthread_create(&recv_thread_id, 99, 1, &recv_thread, 0);
  //    prio_pthread_create(&recv_thread_id, SCHED_RR, 1, &recv_thread, 0);
  //  prio_pthread_create(&recv_thread_id, SCHED_OTHER, 0, &recv_thread, 0);

  fprintf(stderr,"made pthread\n");
  
  pktidx_prev=0;   
  int pc=0;  
  while(run) {
    if (r_pos==w_pos) {
      pthread_mutex_lock(&recv_mutex);
      //printf("mutex lock\n");
      while (r_pos==w_pos) pthread_cond_wait(&recv_cond, &recv_mutex);
      pthread_mutex_unlock(&recv_mutex);
      //      printf("mutex unlock\n");
    }

    pktidx_prev++;
    p=buf+BUFSIZE*w_pos;
    //    pktidx=(((uint16_t)p[1])<<8)|p[0];
    pktidx=(((uint16_t)p[11])<<8)|p[10];
    if (pc==0) {
      pc=1;
      printf("first idx: %x\n",pktidx);
    } else if (pktidx!=pktidx_prev ) { //&& !reset_pointers
      uint16_t a=pktidx-pktidx_prev;
      lost+=a;     
      printf("mismatching sequence %u: %x vs %x. %u packets lost in total\n",++mismatch,pktidx,pktidx_prev,lost);
    }  
    reset_pointers = 0;
    
    d=(r_pos-w_pos)%BUFNUM;
    if (d>max_buf_use) max_buf_use=d;

   
    pktidx_prev=pktidx;

    w_pos=(w_pos+1)%BUFNUM;
  }  
  printf("C\n");

  pthread_join(recv_thread_id,0);

  printstat();
  free(buf);
  return 0;
}
