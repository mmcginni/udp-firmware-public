#!/bin/sh
logfile=$1
rootfile=`echo $logfile | sed -s 's,log,root,'`

while read line; do
    rrate=`echo $line | awk '{print $8}'`
    wrate=`echo $line | awk '{print $12}'`
    tstamp=`echo $line | awk '{print $15}'`
    totpkt=`echo $line | awk '{print $17}'`
    totgb=`echo $line | awk '{print $18}' | sed 's,(,,;s,GB.*,,'`                
    mismatch=`echo $line | awk '{print $21}'`
    rateOH=`echo $line | awk '{print $23}'`    
    echo "$rrate:$wrate:$tstamp:$totpkt:$totgb:$mismatch:$rateOH"

    #echo $line
    
done < <(grep wpkt $logfile) > nvme.tmp

root -l -q -b nvmeOH.C\(\"nvme.tmp\",\"$rootfile\"\)
