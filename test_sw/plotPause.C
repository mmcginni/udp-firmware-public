int plotPause()  {

    TFile * f = new TFile("pause.root");
  //  TFile * f = new TFile("pause_vary.root");
  TGraph * g = (TGraph *)(f->Get("pause"));

  g->SetMarkerStyle(20);
  g->SetMarkerSize(0.2);
  g->SetMarkerColor(kRed);
  g->SetLineWidth(2);
  g->SetLineColor(kBlue);      

  TCanvas * c = new TCanvas("c","c",0,0,800,600);
  float maxX = TMath::MaxElement(g->GetN(),g->GetX());
  float maxY = TMath::MaxElement(g->GetN(),g->GetY());
  TH1F * frame = (TH1F*)(c->DrawFrame(0,0,maxX,11.));
  frame->GetXaxis()->SetTitle("time [A.U.]");
  frame->GetYaxis()->SetTitle("rate [Gbps]");  
  g->Draw("L");

  c->Print("pause_freq.png");
  //  c->Print("pause_timeout.png");
  
  return 0;
}
