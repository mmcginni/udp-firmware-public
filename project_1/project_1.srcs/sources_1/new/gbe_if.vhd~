
--to map pin locations to gtx tranceiver coordinates see  page 357 of 
--https://www.xilinx.com/support/documentation/user_guides/ug476_7Series_Transceivers.pdf

--FMC12
--DP0: MGTXTXP2_118 D10,D9,B10,B9       GTXE2_CHANNEL_X0Y30
--DP1: MGTXRXP3_118 C12,C11, A12,A11    GTXE2_CHANNEL_X0Y31
--DP2: MGTXRXP0_118 F10,F9,C8,C7        GTXE2_CHANNEL_X0Y28
--DP3: MGTXRXP1_117 E4,E3,B2,B1         GTXE2_CHANNEL_X0Y25
  

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
-- KH
use ieee.std_logic_misc.all;
--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity gbe_if is

    Port ( 
    	clk					: in 	std_logic;
	    reset					: in 	std_logic;
	    ipb_mosi_i			: in 	ipb_wbus;
	    ipb_miso_o			: out ipb_rbus;
	    refclk_p,refclk_n   : in std_logic;
	    txp,txn             : out std_logic;
	    rxp,rxn             : in std_logic
	    );

end gbe_if;

architecture Behavioral of gbe_if is

 component ten_gig_eth_pcs_pma_0_support is
    port (
      refclk_p             : in  std_logic;
      refclk_n             : in  std_logic;

      coreclk_out      : out std_logic;
      reset                : in  std_logic;
      sim_speedup_control  : in  std_logic := '0';
      qplloutclk_out       : out std_logic;
      qplloutrefclk_out    : out std_logic;
      qplllock_out         : out std_logic;
      rxrecclk_out         : out std_logic;
      txusrclk_out         : out std_logic;
      txusrclk2_out        : out std_logic;
      gttxreset_out        : out std_logic;
      gtrxreset_out        : out std_logic;
      txuserrdy_out        : out std_logic;
      areset_datapathclk_out     : out std_logic;
      reset_counter_done_out : out std_logic;
      xgmii_txd            : in  std_logic_vector(31 downto 0);
      xgmii_txc            : in  std_logic_vector(3 downto 0);
      xgmii_rxd            : out std_logic_vector(31 downto 0);
      xgmii_rxc            : out std_logic_vector(3 downto 0);
      txp                  : out std_logic;
      txn                  : out std_logic;
      rxp                  : in  std_logic;
      rxn                  : in  std_logic;
      resetdone_out        : out std_logic;
      signal_detect        : in  std_logic;
      tx_fault             : in  std_logic;
      tx_disable           : out std_logic;
      configuration_vector : in  std_logic_vector(535 downto 0);
      status_vector        : out std_logic_vector(447 downto 0);
      pma_pmd_type         : in std_logic_vector(2 downto 0);
      core_status          : out std_logic_vector(7 downto 0));
  end component;
  
  
  COMPONENT gbe_test_fifo
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(35 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
  
COMPONENT udp_header_mem
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;  
  

  signal coreclk       : std_logic;
  signal resetdone_int : std_logic;

  signal qplloutclk_out : std_logic;
  signal qplloutrefclk_out : std_logic;
  signal qplllock_out : std_logic;

  signal txusrclk_out : std_logic;
  signal txusrclk2_out : std_logic;
  signal gttxreset_out        : std_logic;
  signal gtrxreset_out        : std_logic;
  signal txuserrdy_out        : std_logic;
  signal areset_datapathclk_out     : std_logic;

  signal reset_counter_done_out : std_logic;

  signal xgmii_txd_reg : std_logic_vector(31 downto 0) := x"00000000";
  signal xgmii_txc_reg : std_logic_vector(3 downto 0) := x"0";

  signal xgmii_rxd_int : std_logic_vector(31 downto 0);
  signal xgmii_rxc_int : std_logic_vector(3 downto 0);

  signal xgmii_txd_tmp : std_logic_vector(31 downto 0) := x"00000000";
  signal xgmii_txc_tmp : std_logic_vector(3 downto 0) := x"0";
  signal xgmii_txd_tmp2 : std_logic_vector(31 downto 0) := x"00000000";
  signal xgmii_txc_tmp2 : std_logic_vector(3 downto 0) := x"0";

  signal xgmii_rxd_tmp : std_logic_vector(31 downto 0) := x"00000000";
  signal xgmii_rxc_tmp : std_logic_vector(3 downto 0) := x"0";
  signal xgmii_rxd_tmp2 : std_logic_vector(31 downto 0) := x"00000000";
  signal xgmii_rxc_tmp2 : std_logic_vector(3 downto 0) := x"0";

  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of xgmii_txd_tmp : signal is "no";
  attribute SHREG_EXTRACT of xgmii_txc_tmp : signal is "no";
  attribute SHREG_EXTRACT of xgmii_txd_tmp2 : signal is "no";
  attribute SHREG_EXTRACT of xgmii_txc_tmp2 : signal is "no";
  attribute SHREG_EXTRACT of xgmii_rxd_tmp : signal is "no";
  attribute SHREG_EXTRACT of xgmii_rxc_tmp : signal is "no";
  attribute SHREG_EXTRACT of xgmii_rxd_tmp2 : signal is "no";
  attribute SHREG_EXTRACT of xgmii_rxc_tmp2 : signal is "no";
  signal configuration_vector : std_logic_vector(535 downto 0) := (others => '0');
  signal status_vector : std_logic_vector(447 downto 0);


  signal pma_link_status,rx_sig_det,pcs_rx_link_status,pcs_rx_locked,pcs_hiber,teng_pcs_rx_link_status,pcs_rx_hiber_lh,pcs_rx_locked_ll : std_logic;
  signal pma_reset, pcs_reset,gbe_reset : std_logic;

  signal pcs_err_block_count :  std_logic_vector(7 downto 0);
  signal pcs_ber_count : std_logic_vector(5 downto 0);
  signal pcs_test_patt_err_count : std_logic_vector(15 downto 0);
  signal core_status          :  std_logic_vector(7 downto 0);

  signal  xgmii_txc,  xgmii_rxc : std_logic_vector(3 downto 0);
  signal  xgmii_txd,  xgmii_rxd : std_logic_vector(31 downto 0);


  signal ipbus_ack : std_logic;
  
  
signal tx_fifo_wr, tx_fifo_rd, tx_fifo_full, tx_fifo_empty,tx_fifo_aempty : std_logic;
signal rx_fifo_wr, rx_fifo_rd, rx_fifo_full, rx_fifo_empty,rx_fifo_empty_i : std_logic;
signal rx_fifo_din,rx_fifo_dout : std_logic_vector(35 downto 0);
signal tx_fifo_din,tx_fifo_dout : std_logic_vector(35 downto 0);

signal tx_crc_en,tx_crc_rst,tx_crc_sel : std_logic;
signal tx_crc_out : std_logic_vector(31 downto 0);
signal tx_crc_out_b : std_logic_vector(31 downto 0);


signal tx_state: std_logic_vector(3 downto 0);
signal rx_state: std_logic_vector(2 downto 0);

signal tx_fifo_strobe, tx_fifo_strobe_i : std_logic;
signal tx_fifo_c : std_logic_vector(1 downto 0);
signal tx_fifo_busy : std_logic;

signal trig: std_logic;
signal trig_rx_i,trig_rx_j,trig_rx_k : std_logic;

signal trig_i : std_logic_vector(7 downto 0);
signal tx_buf : std_logic_vector(31 downto 0);

signal rx_auto,rx_auto_i : std_logic;

signal coreclk_c : std_logic_vector(31 downto 0);
signal coreclk_c1 : std_logic_vector(31 downto 0);
signal coreclk_i,coreclk_j : std_logic; 	


signal udp_header_mem_a_we : std_logic;

signal udp_header_mem_a_din : std_logic_vector(31 downto 0);
signal udp_header_mem_b_dout : std_logic_vector(31 downto 0);

signal udp_header_mem_a_addr : std_logic_vector(8 downto 0);
signal udp_header_mem_b_addr : std_logic_vector(8 downto 0);
signal pkt_ip_count : std_logic_vector(15 downto 0);
signal tx_pkt_state : std_logic_vector(3 downto 0);

signal pkt_payload_c, pkt_payload_length : std_logic_vector(15 downto 0);

signal tx_pkt_strobe, tx_pkt_strobe_i : std_logic;

signal rx_pkt_strobe,rxtx_echo : std_logic;
signal tx_pause,tx_pause_ena : std_logic;
signal pause_c : std_logic_vector(19 downto 0);
signal pause_f_c : std_logic_vector(15 downto 0);
signal rx_c : std_logic_vector(15 downto 0);
signal pause_t : std_logic_vector(31 downto 0);
signal rx_pause_frame_flag : std_logic;

signal tx_pkt_n, tx_pkt_w : std_logic_vector(31 downto 0);
signal tx_pkt_n_c, tx_pkt_w_c: std_logic_vector(31 downto 0);


component crc 
  port ( data_in_e : in std_logic_vector (31 downto 0);
    crc_en , rst, clk : in std_logic;
    crc_out : out std_logic_vector (31 downto 0));
end component;

--KH
signal clks_div : std_logic_vector(1 downto 0);
signal clkdiv_ipb_r : ipb_rbus;
signal clkdiv_ipb_w : ipb_wbus;
signal clk_counter : std_logic_vector( 31 downto 0);
signal sps_counter : std_logic_vector( 1 downto 0);
signal duty_cycle : std_logic_vector(2 downto 0);
signal wide_counter : std_logic_vector( 63 downto 0);
begin

 -- resetdone <= resetdone_int;

   configuration_vector(0)   <= '0'; --pma_loopback;
   configuration_vector(15)  <= pma_reset;
   configuration_vector(16)  <= '0'; -- global_tx_disable;
   configuration_vector(110) <= '0'; --pcs_loopback;
   configuration_vector(111) <= pcs_reset;
   configuration_vector(169 downto 112) <= (others => '0'); -- test_patt_a_b;
   configuration_vector(233 downto 176) <= (others => '0'); -- test_patt_a_b;
   configuration_vector(240) <= '0'; --data_patt_sel;
   configuration_vector(241) <= '0'; --test_patt_sel;
   configuration_vector(242) <= '0'; --rx_test_patt_en;
   configuration_vector(243) <= '0'; --tx_test_patt_en;
   configuration_vector(244) <= '0'; --prbs31_tx_en;
   configuration_vector(245) <= '0'; --prbs31_rx_en;
   configuration_vector(399 downto 384) <= x"4C4B";
   configuration_vector(512) <= '0'; --set_pma_link_status;
   configuration_vector(516) <= '0'; --set_pcs_link_status;
   configuration_vector(518) <= '0'; --clear_pcs_status2;
   configuration_vector(519) <= '0'; --clear_test_patt_err_count;

   pma_link_status <= status_vector(18);
   rx_sig_det <= status_vector(48);
   pcs_rx_link_status <= status_vector(226);
   pcs_rx_locked <= status_vector(256);
   pcs_hiber <= status_vector(257);
   teng_pcs_rx_link_status <= status_vector(268);
   pcs_err_block_count <= status_vector(279 downto 272);
   pcs_ber_count <= status_vector(285 downto 280);
   pcs_rx_hiber_lh <= status_vector(286);
   pcs_rx_locked_ll <= status_vector(287);
   pcs_test_patt_err_count <= status_vector(303 downto 288);


  -- Add a pipeline to the xmgii_tx inputs, to aid timing closure
  tx_reg_proc : process(coreclk)
  begin
    if(coreclk'event and coreclk = '1') then
      xgmii_txd_tmp <= xgmii_txd;
      xgmii_txc_tmp <= xgmii_txc;
      xgmii_txd_tmp2 <= xgmii_txd_tmp;
      xgmii_txc_tmp2 <= xgmii_txc_tmp;
      xgmii_txd_reg <= xgmii_txd_tmp2;
      xgmii_txc_reg <= xgmii_txc_tmp2;
    end if;
  end process;

  -- Add a pipeline to the xmgii_rx outputs, to aid timing closure
  rx_reg_proc : process(coreclk)
  begin
    if(coreclk'event and coreclk = '1') then
      xgmii_rxd_tmp <= xgmii_rxd_int;
      xgmii_rxc_tmp <= xgmii_rxc_int;
      xgmii_rxd_tmp2 <= xgmii_rxd_tmp;
      xgmii_rxc_tmp2 <= xgmii_rxc_tmp;
      xgmii_rxd <= xgmii_rxd_tmp2;
      xgmii_rxc <= xgmii_rxc_tmp2;
    end if;
  end process;



--  dclk_bufg_i : BUFG
--  port map
--  (
--     I => dclk,
--     O => dclk_buf
--  );

  -- Instance the 10GBASE-KR Core Support layer
  --modifications: use a BUFR to generate dclk from refclk/2 internally
  ten_gig_eth_pcs_pma_core_support_layer_i : ten_gig_eth_pcs_pma_0_support
    port map (
      refclk_p            => refclk_p,
      refclk_n            => refclk_n,

      coreclk_out         => coreclk,
      reset               => gbe_reset,
      sim_speedup_control => '0',
      qplloutclk_out      => qplloutclk_out,
      qplloutrefclk_out   => qplloutrefclk_out,
      qplllock_out        => qplllock_out,
      rxrecclk_out        => open,
      txusrclk_out        => txusrclk_out,
      txusrclk2_out       => txusrclk2_out,
      gttxreset_out       => gttxreset_out,
      gtrxreset_out       => gtrxreset_out,
      txuserrdy_out       => txuserrdy_out,
      areset_datapathclk_out  => areset_datapathclk_out,
     reset_counter_done_out => reset_counter_done_out,
      xgmii_txd           => xgmii_txd_reg,
      xgmii_txc           => xgmii_txc_reg,
      xgmii_rxd           => xgmii_rxd_int,
      xgmii_rxc           => xgmii_rxc_int,
      txp                 => txp,
      txn                 => txn,
      rxp                 => rxp,
      rxn                 => rxn,
      resetdone_out       => resetdone_int,
      signal_detect       => '1',
      tx_fault            => '0',
      tx_disable          => open,
      configuration_vector => configuration_vector,
      status_vector       => status_vector,
      pma_pmd_type        => "101",
      core_status         => core_status);


  process(coreclk) 
  begin
  end process;




gbt_rx_fifo : gbe_test_fifo
  PORT MAP (
    rst => areset_datapathclk_out,
    wr_clk => coreclk,
    rd_clk => clk,
    din => rx_fifo_din,
    wr_en => rx_fifo_wr,
    rd_en => rx_fifo_rd,
    dout => rx_fifo_dout,
    full => rx_fifo_full,
    empty => rx_fifo_empty,
    almost_empty => open,
    wr_rst_busy => open,
    rd_rst_busy => open
  );  


gbt_tx_fifo_i : gbe_test_fifo
  PORT MAP (
    rst => areset_datapathclk_out,
    wr_clk => coreclk,
    rd_clk => coreclk,
    din => tx_fifo_din,
    wr_en => tx_fifo_wr,
    rd_en => tx_fifo_rd,
    dout => tx_fifo_dout,
    full => tx_fifo_full,
    empty => tx_fifo_empty,
    almost_empty => tx_fifo_aempty,
    wr_rst_busy => open,
    rd_rst_busy => open
  );  

 crc_i : crc
    port map (
     crc_en => tx_crc_en,
     rst => tx_crc_rst,
     clk => coreclk,
     data_in_e => tx_fifo_dout(31 downto 0),
     crc_out => tx_crc_out );
     

 -- Receive packets from PHY, store to rx_fifo, decode pause frames
    process(coreclk, areset_datapathclk_out)
      variable a : std_logic_vector(31 downto 0);
    begin
      if areset_datapathclk_out='1' then
        rx_fifo_wr<='0';
        
        trig_rx_i<='0';        
        trig_rx_j<='0';
        trig_rx_k<='0';
        rx_state<="000";
        
        rx_auto_i<='0';
        
        rx_pkt_strobe<='0';        
 
        tx_pause<='0';
        pause_c<=(others => '0');
        pause_f_c<=(others => '0');

      elsif rising_edge(coreclk) then
        trig_rx_i<=trig;
        trig_rx_j<=trig_rx_i;
        trig_rx_k<=trig_rx_j;

        rx_auto_i<=rx_auto;

        rx_pkt_strobe<='0';        
        
        rx_fifo_din<=xgmii_rxc&xgmii_rxd;

        tx_pause<='0';        
        if unsigned(pause_c)/=0 then
          tx_pause<='1';
          pause_c<=std_logic_vector(unsigned(pause_c)-1);
        end if;
        
        case rx_state is 
          when "000" => 
           
           rx_fifo_empty_i<=rx_fifo_empty;
            
           if xgmii_rxd(7 downto 0)=X"FB" and xgmii_rxc(0)='1' then
             if  rx_fifo_empty_i='1' then 
               rx_fifo_wr<='1';           
             end if;  
             rx_state<="001";
             rx_c<=(others => '0');
           end if;
            
          when "001" =>
          
            rx_c<=std_logic_vector(unsigned(rx_c)+1);
            rx_pause_frame_flag<='0';
            if unsigned(rx_c)=4 and xgmii_rxd(31 downto 0)=X"01000888" then
              rx_pause_frame_flag<='1';
            end if;
            
            if unsigned(rx_c)=5 and rx_pause_frame_flag='1' then
              pause_f_c<=std_logic_vector(unsigned(pause_f_c)+1);
              pause_c<=xgmii_rxd(7 downto 0) & xgmii_rxd(15 downto 8) & "0000";
              a:=X"0000" & xgmii_rxd(7 downto 0) & xgmii_rxd(15 downto 8);
              pause_t<=std_logic_vector(unsigned(pause_t)+ unsigned(a) );              
            end if;
            
  
            if (xgmii_rxd(7 downto 0)=X"FD" and xgmii_rxc(0)='1') or (xgmii_rxd(15 downto 8)=X"FD" and xgmii_rxc(1)='1') or (xgmii_rxd(23 downto 16)=X"FD" and xgmii_rxc(2)='1') or (xgmii_rxd(31 downto 24)=X"FD" and xgmii_rxc(3)='1')then
              rx_state<="011";
            end if;    
            if rx_fifo_full='1' then
              rx_fifo_wr<='0';
            end if;
            
            
          when "011" =>    
            rx_pkt_strobe<='1';
            rx_fifo_wr<='0';        
            rx_state<="000";
            
          when others => 
            rx_state<="000";
            
        end case;
        
        
        
        
         
      end if;
   end process;      





coreclk_sample : entity work.reg_sync 
    Generic map (width => 0 , auto=>0)
    Port map ( clk_a => clk,
               clk_b =>coreclk,
               in_a => "",
               out_b => open,
               strobe_a => coreclk_i,
               ready_a  => open,
               strobe_b => coreclk_j);
  


tx_fifo_strobe_i<=tx_fifo_strobe;
  
      
    process(coreclk)
    begin
      if rising_edge(coreclk) then
        coreclk_c<=std_logic_vector(unsigned(coreclk_c)+1);
        if coreclk_j='1' then
          coreclk_c1<=coreclk_c;
        end if;
       end if;
     end process; 
  

-- add framing and CRC to data being transmitted, push to PHY
    process(coreclk, areset_datapathclk_out)
    variable tx_fifo_dec : std_logic;
    begin
      if areset_datapathclk_out='1' then
    
        tx_fifo_rd<='0';
        tx_state<="0000";        
        
        xgmii_txc<="0001";
        xgmii_txd<=X"000000BC";
        
        tx_crc_rst<='1';
        tx_crc_en<='0';

        tx_fifo_c<="00";
        tx_fifo_busy<='0';

   
                     
      elsif rising_edge(coreclk) then
      
        tx_fifo_rd<='0';
        
        xgmii_txc<="1111"; --idle
        xgmii_txd<=X"07070707";
        
        tx_fifo_dec:='0';
        
        case tx_state is
          when "0000" => --idle, but also final IFG state
            if tx_fifo_c/="00" then
              tx_state<="0001";           
            end if;
         
          when "0001" =>   
            xgmii_txc<="0001";
            xgmii_txd<=X"555555FB"; --start and preamble
            tx_state<="0010";
            tx_fifo_rd<='1';
            
            tx_crc_rst<='0'; --since we have a fwft fifo the first word is alredy available now, but start processing on next cycle so that we can process all words consecutively
            tx_crc_en<='1';



          when "0010" =>   
            xgmii_txc<="0000";
            xgmii_txd<=X"d5555555"; --preamble and SFD
            tx_buf <=tx_fifo_dout(31 downto 0); --pipeline stage in case crc is slow
            tx_fifo_rd<='1';
            tx_state<="0011";
                        
          when "0011" =>
            if tx_fifo_dout(32)='1' then --this is the last word to transmit (the fifo should never run empty, but just in case) 
              tx_state<="0100";

              tx_crc_rst<='1';  --the CRC is currently processing the last word (currently on tx_fifo_dout), so stop at the next cycle
              tx_crc_en<='0';            
              tx_fifo_dec:='1';
            else
              tx_fifo_rd<='1';  
            end if;
            
            
            xgmii_txc<="0000";
            xgmii_txd<=tx_buf;
            tx_buf <=tx_fifo_dout(31 downto 0); --buffer one cycle so that we have the CRC available once all data is out

           
          when "0100" => 
            xgmii_txc<="0000";
            
            tx_crc_out_b<=tx_crc_out;
            xgmii_txd<=tx_buf;
            tx_buf<=tx_crc_out;
            
            if tx_crc_sel='1' then
              tx_state<="0101";
            else
              tx_state<="0111";
            end if;  

          when "0101" => 
            xgmii_txc<="0000";
            xgmii_txd<=tx_buf;
            tx_state<="0110";

            
          when "0110" =>
            xgmii_txc<="1111";
            xgmii_txd<=X"070707FD";    --data complete and idle
            tx_state<="0111";


          when "0111" => --interpacket gap1 (since the data complete state is 4 octets, and the start state will send idle for 4 octets we might not need more gap?
            tx_state<="1000";            

          when "1000" => --interpacket gap2
            tx_state<="0000";            
            
          when others => 
            tx_state<="0000";            
               
        end case;      


        if tx_fifo_strobe_i='1' then  
          if tx_fifo_dec='0' then
            tx_fifo_c<=std_logic_vector(unsigned(tx_fifo_c)+1);
            if tx_fifo_c/="00" then
              tx_fifo_busy<='1';
            end if;  
          end if;  
        elsif tx_fifo_dec='1' then 
--          if unsigned(tx_fifo_c)<2 then --if the packet generator waits for 1+ cycles between pulsing the strobe and testing busy this shoudln't happen
            tx_fifo_busy<='0';
--          end if;  
          tx_fifo_c<=std_logic_vector(unsigned(tx_fifo_c)-1);        
        end if;  


  
      end if;
    end process;      




--packet generator, for now also in coreclk domain
process(coreclk, areset_datapathclk_out)
begin
  if areset_datapathclk_out='1' then
    tx_fifo_strobe<='0';
    udp_header_mem_b_addr<=(others => '0');
    pkt_ip_count<=(others => '0');    
    tx_pkt_state<=(others => '0');
    tx_fifo_wr<='0'; 

   
  elsif rising_edge(coreclk) then

    tx_fifo_wr<='0'; 
    tx_fifo_strobe<='0';

    
    
    if  unsigned(tx_pkt_w_c)/=0 then
      tx_pkt_w_c<=std_logic_vector(unsigned(tx_pkt_w_c)-1); 
    end if;
    
    if tx_pkt_strobe_i='1' or (rx_pkt_strobe='1' and rxtx_echo='1') then
      tx_pkt_n_c<=tx_pkt_n;
      tx_pkt_w_c<=(others => '0');
    end if;  
 
    

    case tx_pkt_state is
      when "0000" =>  
        if( unsigned(sps_counter) >= unsigned(duty_cycle) ) then --KH
          if (tx_pause='0' or tx_pause_ena='0') and tx_fifo_busy='0' and unsigned(tx_pkt_n_c)/=0  and  unsigned(tx_pkt_w_c)=0  then
            --at this point the address is already 0, so start incrementing 
            tx_pkt_state<="0001";
            udp_header_mem_b_addr<=std_logic_vector(unsigned(udp_header_mem_b_addr)+1); 
            
            tx_pkt_n_c<=std_logic_vector(unsigned(tx_pkt_n_c)-1); 
            tx_pkt_w_c<=tx_pkt_w;
          end if;  
        else
          tx_pkt_state<="0000";
        end if;

      when "0001" =>  
        tx_pkt_state<="0010";
        --here the ram sees addr+1, so after this rising edge the data is driven to the register, this is the last cycle where we see addr0
          udp_header_mem_b_addr<=std_logic_vector(unsigned(udp_header_mem_b_addr)+1); 

      when "0010" =>  
        tx_fifo_din(31 downto 0)<=udp_header_mem_b_dout;
        udp_header_mem_b_addr<=std_logic_vector(unsigned(udp_header_mem_b_addr)+1); 
        -- if unsigned(udp_header_mem_b_addr)=12 then --this needs to be the header length +1, this is where we send the last word of the header, plus the packet counter 
        --    tx_pkt_state<="0011";
        --    udp_header_mem_b_addr<=(others => '0'); --reset to 0 now so that we're ready to start generating data immediately from the idle state
        --    tx_fifo_din(31 downto 16)<=pkt_ip_count; --stick this here so that the rest of the payload can be 32-bit aligned
        --    pkt_ip_count<=std_logic_vector(unsigned(pkt_ip_count)+1);
        -- end if;
        if unsigned(udp_header_mem_b_addr)=12 then --this needs to be the header length +1, this is where we send the last word of the header, plus the packet counter 
           tx_fifo_din(31 downto 16)<=x"babe"; --stick this here so that the rest of the payload can be 32-bit aligned
        end if;
        if unsigned(udp_header_mem_b_addr)=13 then --this needs to be the header length +1, this is where we send the last word of the header, plus the packet counter 
           tx_fifo_din(31 downto 0)<=wide_counter(63 downto 32);--x"12345678";--
        end if;
        if unsigned(udp_header_mem_b_addr)=14 then --this needs to be the header length +1, this is where we send the last word of the header, plus the packet counter 
           tx_pkt_state<="0011";
           udp_header_mem_b_addr<=(others => '0'); --reset to 0 now so that we're ready to start generating data immediately from the idle state
           tx_fifo_din(31 downto 0)<=wide_counter(31 downto 0);--x"9abcdef0";
           pkt_ip_count<=std_logic_vector(unsigned(pkt_ip_count)+1);
        end if;                
        pkt_payload_c<=pkt_payload_length;
        
        tx_fifo_din(32)<='0';
        tx_fifo_wr<='1'; 

      when "0011" =>  
        tx_fifo_din(15 downto 0)<=pkt_ip_count; --dummy data
        tx_fifo_din(31 downto 16)<=pkt_ip_count; --dummy data
        tx_fifo_din(32)<='0';
        tx_fifo_wr<='1'; 

        if unsigned(pkt_payload_c)=0 then
          tx_pkt_state<="0100";
          tx_fifo_din(32)<='1'; 
          tx_fifo_strobe<='1';
        end if;
        pkt_payload_c<=std_logic_vector(unsigned(pkt_payload_c)-1);

 
      when "0100" =>  --idle a bit so that the busy flag can take effect by the time we reach state 0
        tx_pkt_state<="0101";

      when "0101" =>  --idle a bit so that the busy flag can take effect by the time we reach state 0
        tx_pkt_state<="0110";

      when "0110" =>  --idle a bit so that the busy flag can take effect by the time we reach state 0
        tx_pkt_state<="0000";

        


      when others => 
        tx_pkt_state<=(others => '0');

    end case;

  end if;
end process;




--memory for storing UDP header (uploaded over IPbus)
header_mem : udp_header_mem
  PORT MAP (
    clka => clk,
    ena => '1',
    wea(0) => udp_header_mem_a_we,
    addra => udp_header_mem_a_addr,
    dina => udp_header_mem_a_din,
    douta => open,
    clkb => coreclk,
    enb => '1',
    web => "0",
    addrb => udp_header_mem_b_addr,
    dinb => X"00000000",
    doutb => udp_header_mem_b_dout
  );


tx_fifo_str : entity work.reg_sync 
    Generic map (width => 0 , auto=>0)
    Port map ( clk_a => clk,
               clk_b =>coreclk,
               in_a => "",
               out_b => open,
               strobe_a => tx_pkt_strobe,
               ready_a  => open,
               strobe_b => tx_pkt_strobe_i);






  

--configuration interface (IPbus)
  process(clk, reset)
  begin
    if reset='1' then
      ipbus_ack      <= '0';
  
      rx_fifo_rd<='0';
           
      trig_i<="00000000";
      trig<='0';
      
      rx_auto<='0';
      
      gbe_reset <='1';
      pma_reset <='0'; 
      pcs_reset <='0';       
      
      
      udp_header_mem_a_we<='0';
      tx_pkt_strobe<='0';
      
      tx_pause_ena<='0';

      rxtx_echo<='0';
                   
    elsif rising_edge(clk) then
      ipbus_ack<='0';
      
      coreclk_i<='0';

      trig_i<="0"&trig_i(7 downto 1);
      trig<=trig_i(0); 

      rx_fifo_rd<='0';
      
      
      udp_header_mem_a_we<='0';
      tx_pkt_strobe<='0';

      

 -- write
      if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='1' then
           
        case ipb_mosi_i.ipb_addr(4 downto 0) is  --cfg
                
          when "00010" => 
            trig_i<=X"ff";
            ipbus_ack<='1';
            
          when "00011" =>             
          
            tx_crc_sel<=ipb_mosi_i.ipb_wdata(16); 
          
            gbe_reset <=ipb_mosi_i.ipb_wdata(0); 
            pma_reset <=ipb_mosi_i.ipb_wdata(1); 
            pcs_reset <=ipb_mosi_i.ipb_wdata(2); 
          
            
            rx_auto<=ipb_mosi_i.ipb_wdata(8);

            rxtx_echo<=ipb_mosi_i.ipb_wdata(16);
            tx_pause_ena<=ipb_mosi_i.ipb_wdata(24);

            
            ipbus_ack<='1';            
            

         when "00100" =>              --lazy
           udp_header_mem_a_addr<=ipb_mosi_i.ipb_wdata(8 downto 0);
           ipbus_ack<='1';            

         
         when  "00101" => 
          
           udp_header_mem_a_din<=ipb_mosi_i.ipb_wdata;
           udp_header_mem_a_we<='1';
           ipbus_ack<='1';            


         when "00110" => 
           pkt_payload_length <=       ipb_mosi_i.ipb_wdata(15 downto 0);
           tx_pkt_strobe<='1';
           ipbus_ack<='1';            


         when "01000" => 
           tx_pkt_n <=       ipb_mosi_i.ipb_wdata(31 downto 0);
           ipbus_ack<='1';            

         when "01001" => 
           tx_pkt_w <=       ipb_mosi_i.ipb_wdata(31 downto 0);
           ipbus_ack<='1';            

  --KH
         when "01100" => 
           duty_cycle <=       ipb_mosi_i.ipb_wdata(2 downto 0);
           ipbus_ack<='1';            


          when "10000" => 
            clkdiv_ipb_w.ipb_addr <= ipb_mosi_i.ipb_addr;
            clkdiv_ipb_w.ipb_wdata <= ipb_mosi_i.ipb_wdata(31 downto 0);
            clkdiv_ipb_w.ipb_strobe <= '1';
            clkdiv_ipb_w.ipb_write <= '1';                        
            ipbus_ack <='1';
          when "10001" => 
            clkdiv_ipb_w.ipb_addr <= ipb_mosi_i.ipb_addr;
            clkdiv_ipb_w.ipb_wdata <= ipb_mosi_i.ipb_wdata(31 downto 0);
            clkdiv_ipb_w.ipb_strobe <= '1';
            clkdiv_ipb_w.ipb_write <= '1';                        
            ipbus_ack<='1';            
--KH            
              when others => 
          ipbus_ack<='1';              
      end case;  
    end if;  
          
--read
    if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='0' then
      ipb_miso_o.ipb_rdata<=(others => '0');
      case ipb_mosi_i.ipb_addr(4 downto 0) is
        when "00000" => 
          ipb_miso_o.ipb_rdata(31 downto 0) <= rx_fifo_dout(31 downto 0);
          --ipb_miso_o.ipb_rdata(24) <= rx_fifo_empty;
          rx_fifo_rd<='1'; 
          ipbus_ack<='1';   

        when "00001" => 
          ipb_miso_o.ipb_rdata(3 downto 0) <= rx_fifo_dout(35 downto 32);
          ipb_miso_o.ipb_rdata(16) <= rx_fifo_empty;          
          ipbus_ack<='1';   


        when "00010" => 

          ipb_miso_o.ipb_rdata(2 downto 0)<=rx_state;
          ipb_miso_o.ipb_rdata(3) <= rx_fifo_full;
          ipb_miso_o.ipb_rdata(4) <= rx_fifo_empty;
          
          ipb_miso_o.ipb_rdata(5) <=gbe_reset;
          
          ipb_miso_o.ipb_rdata(6) <=rx_auto;

          ipb_miso_o.ipb_rdata(10 downto 8)<=tx_state(2 downto 0);
          ipb_miso_o.ipb_rdata(11) <= tx_fifo_full;
          ipb_miso_o.ipb_rdata(12) <= tx_fifo_empty;
          
          
          ipb_miso_o.ipb_rdata(13)<=reset_counter_done_out;
          ipb_miso_o.ipb_rdata(14)<=areset_datapathclk_out;
          ipb_miso_o.ipb_rdata(15)<= resetdone_int;
          
          ipb_miso_o.ipb_rdata(16)<=pma_link_status;
          ipb_miso_o.ipb_rdata(17)<=rx_sig_det;
          ipb_miso_o.ipb_rdata(18)<=pcs_rx_link_status;
          ipb_miso_o.ipb_rdata(19)<=pcs_rx_locked;
          ipb_miso_o.ipb_rdata(20)<=pcs_hiber;
          ipb_miso_o.ipb_rdata(21)<=teng_pcs_rx_link_status;
          ipb_miso_o.ipb_rdata(22)<=pcs_rx_hiber_lh;
          ipb_miso_o.ipb_rdata(23)<=pcs_rx_locked_ll; 
          
          ipb_miso_o.ipb_rdata(31 downto 24)<=core_status;
          
                 
          ipbus_ack<='1';   

          
        when "00011" => 
           ipb_miso_o.ipb_rdata(3 downto 0)<=tx_pkt_state;
           ipb_miso_o.ipb_rdata(5 downto 4)<=tx_fifo_c;
           ipb_miso_o.ipb_rdata(11 downto 8)<=tx_state;
 
           ipb_miso_o.ipb_rdata(31 downto 24)<=udp_header_mem_b_addr(7 downto 0);
           ipb_miso_o.ipb_rdata(23 downto 16)<=pkt_payload_c(7 downto 0);
           ipbus_ack<='1';   


        when "00100" => 
           ipb_miso_o.ipb_rdata(7 downto 0)<=  pcs_err_block_count;
           ipb_miso_o.ipb_rdata(13 downto 8)<= pcs_ber_count;
           ipb_miso_o.ipb_rdata(31 downto 16)<= pcs_test_patt_err_count;
           ipbus_ack<='1';   

        when "00101" => 
           ipb_miso_o.ipb_rdata<=coreclk_c1;
           coreclk_i<='1';
           ipbus_ack<='1';   

        when "00110" => 
           ipb_miso_o.ipb_rdata<=tx_crc_out_b;
           coreclk_i<='1';
           ipbus_ack<='1';   

        when "00111" => 
           ipb_miso_o.ipb_rdata(15 downto 0)<=pause_f_c;
           ipbus_ack<='1';   
  

        when "01000" => 
           ipb_miso_o.ipb_rdata<=pause_t;
           ipbus_ack<='1';   
     
        -- KH

        when "01100" => 
          ipb_miso_o.ipb_rdata(2 downto 0) <= duty_cycle;
          ipbus_ack<='1';            

        when "10000" => 
          ipb_miso_o.ipb_rdata <= clkdiv_ipb_r.ipb_rdata;
--          ipbus_ack <= clkdiv_ipb_r.ipb_ack;
          ipbus_ack<='1';  
        when "10001" => 
          ipb_miso_o.ipb_rdata <= clkdiv_ipb_r.ipb_rdata;
--          ipbus_ack <= clkdiv_ipb_r.ipb_ack;
          ipbus_ack<='1';  
        --KH
        when others => 
          ipb_miso_o.ipb_rdata <= x"12345678";
          ipbus_ack<='1';  
                   
        end case;
      end if;
    end if;
  end process;
      
  ipb_miso_o.ipb_ack <= ipbus_ack;
  ipb_miso_o.ipb_err <= '0';

  -- KH
  div : entity work.freq_ctr_div
    generic map(
      N_CLK => 2
      )
    port map(
      clk(0)    => clk,
      clk(1)    => coreclk, --31.25
      clkdiv    => clks_div --312.5
    );

  ctr : entity work.freq_ctr
    generic map(
      N_CLK => 2
      )
    port map(
      clk         => clk,
      rst         => reset,
      ipb_in      => clkdiv_ipb_w,
      ipb_out     => clkdiv_ipb_r,
      clkdiv      => clks_div
      );

  clkcntr:  process(coreclk, areset_datapathclk_out)
  begin
    if( areset_datapathclk_out ) then
      clk_counter <= (others => '0');
      wide_counter <= (others => '0');      
    end if ;
    if(coreclk'event and coreclk = '1') then
      if( clk_counter < x"BA43B740" )  then --10sec
        clk_counter <= std_logic_vector(unsigned(clk_counter) + 1);
      else
        clk_counter <= x"00000000";
      end if;
      wide_counter <= std_logic_vector(unsigned(wide_counter) + 1);
    end if;
  end process;

  sps : process(coreclk,areset_datapathclk_out)
  begin
    if( areset_datapathclk_out ) then
      sps_counter <= (others => '0');
    end if ;
    if(coreclk'event and coreclk = '1') then
      if( or_reduce(clk_counter) = '0' ) then
        sps_counter <= std_logic_vector(unsigned(sps_counter) + 1);
      end if;
    end if;         
    end process;    
end Behavioral;
