----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Johan Borg 
-- 
-- Create Date: 09/29/2017 07:51:11 PM
-- Design Name: 
-- Module Name: eth_switch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Tool for multiplexing between multiple data sources that feed a AXI MAC. Intends to behave exacly like the 1Gbit MAC when connected to a 1Gbit MAC. Results may vary with other MACs, but should be OK if they accept 2 bytes before deasserting ready.  
--              Port A has priority, but the code could relatively easily be externded to use a more balanced access scheme.
--              A_override is inteded for debugging purposes only (forces a permanent connection between port A and the MAC). It definately shoudln't be toggled while port B is transmitting.
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity eth_switch is
    Port (         
-- this is the clock everything is synchronous to, connect to mac_clk_o           
           mac_clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;    
           
-- the interface to the MAC core           
           ext_tx_data : out STD_LOGIC_VECTOR (7 downto 0);
           ext_tx_valid : out STD_LOGIC;
           ext_tx_last : out STD_LOGIC;
           ext_tx_error : out STD_LOGIC;
           ext_tx_ready : in STD_LOGIC;           

--user port 1
           A_tx_data : in STD_LOGIC_VECTOR (7 downto 0);
           A_tx_valid : in STD_LOGIC;
           A_tx_last : in STD_LOGIC;
           A_tx_error : in STD_LOGIC;
           A_tx_ready : out STD_LOGIC;


--user port 2
           B_tx_data : in STD_LOGIC_VECTOR (7 downto 0);
           B_tx_valid : in STD_LOGIC;
           B_tx_last : in STD_LOGIC;
           B_tx_error : in STD_LOGIC;
           B_tx_ready : out STD_LOGIC;

           
--override (not synchronized with packet transmission, should be static
           A_override : in STD_LOGIC           
            );
           
           
           
end eth_switch;

architecture Behavioral of eth_switch is
 
 signal A_state : std_logic_vector(2 downto 0);
 signal A_bus,A0,A1   : std_logic_vector(9 downto 0); --buffers for the first 2 bytes we should grab to properly emulate a MAC
 signal A_tx_request , A_tx_grant,A_tx_ready_int,A_tx_valid_int : std_logic;
 signal B_state : std_logic_vector(2 downto 0);
 signal B_bus,B0,B1   : std_logic_vector(9 downto 0); --buffers for the first 2 bytes we should grab to properly emulate a MAC
 signal B_tx_request , B_tx_grant,B_tx_ready_int,B_tx_valid_int : std_logic;
  
 signal tx_mux : std_logic_vector(1 downto 0);
 signal ext_bus : std_logic_vector(9 downto 0);
 

begin

 statemachine: process (mac_clk_i,rst_i)
 begin
   if rst_i='1' then
     tx_mux<="00";
   
     A_state<=(others =>'0');
     A_tx_valid_int<='0';
     A_tx_ready_int<='0';
     A_tx_request<='0';
     A_tx_grant<='0';
     A0<=(others=>'0');
     A1<=(others=>'0');
      
     B_state<=(others =>'0');
     B_tx_ready_int<='0';
     B_tx_valid_int<='0';     
     B_tx_request<='0';
     B_tx_grant<='0';
     B0<=(others=>'0');
     B1<=(others=>'0');
     
     
   elsif rising_edge(mac_clk_i) then
     case A_state is 
       when "000" =>
         if A_tx_valid='1' then  --it's not valid for the slave to release valid until the end of the packet, so we won't check again
           A_state<="001";           
           A_tx_ready_int<='1'; -- tell the slave we grab the data on the following cycle, so it needs to provide new data 
         end if; 
         
       when "001" =>  --the slave sees tx_valid here and updates to new data
         A1<=A_tx_last&A_tx_error&A_tx_data;
         A_state<="010";
         
       when "010" =>
         A1<=A_tx_last&A_tx_error&A_tx_data;
         A0<=A1;
         A_tx_ready_int<='0';  --tell the slave to hold it's next byte
         A_state<="011";
         A_tx_request<='1'; --request the MAC
         
       when "011" => 
         if A_tx_grant='1' then --it's our turn to transmit!
           tx_mux<="00";  --until this point it may be the B slave's bus (but that doesn't matter for A_tx_ready_int)
           A_tx_valid_int<='1';
           A_state<="100"; 
         end if;
       
       when "100" => 
         if ext_tx_ready='1' then  --eventually the MAC will take our first byte, update to byte 2 
           A0<=A1;           
           A_state<="101";  --but we can't grab more data from the slave yet since once we start we aren't allowed to stop
         end if;
    
       when "101" =>   --the slave grabs our second byte, ext_tx_ready remains high but drops after this edge
         if ext_tx_ready='1' then   --we update the output to hold the slave's current data, but we can't ask for more yet since the MAC will stop here 
           tx_mux<="01"; --mux the slave directly to the MAC so that it can provide data on time once the MAC restarts (it already has a byte available, but once it sees a tx_ready it'll provide new bytes
           A_state<="110";      
         end if;
         
         
       when "110"  =>  
         if A_tx_valid='0' then --must only happen when after  error or last has been asserted
           tx_mux<="00";
           A_state<="000";
           A_tx_valid_int<='0';
           A_tx_request<='0';
         end if;
       when others => 
        null;
      end case; 
      
      
      
      case B_state is 
         when "000" =>
           if B_tx_valid='1' then  --it's not valid for the slave to release valid until the end of the packet, so we won't check again
             B_state<="001";           
             B_tx_ready_int<='1'; -- tell the slave we grab the data on the following cycle, so it needs to provide new data 
           end if; 
           
         when "001" =>  --the slave sees tx_valid here and updates to new data
           B1<=B_bus;
           B_state<="010";
           
         when "010" =>
           B1<=B_bus;
           B0<=B1;
           B_tx_ready_int<='0';  --tell the slave to hold it's next byte
           B_state<="011";
           B_tx_request<='1'; --request the MAC
           
         when "011" => 
           if B_tx_grant='1' then --it's our turn to transmit!
             tx_mux<="10";  --until this point it may be the A slave's bus (but that doesn't matter for B_tx_ready_int)
             B_tx_valid_int<='1';
             B_state<="100"; 
           end if;
         
         when "100" => 
           if ext_tx_ready='1' then  --eventually the MAC will take our first byte, update to byte 2 
             B0<=B1;           
             B_state<="101";  --but we can't grab more data from the slave yet since once we start we aren't allowed to stop
           end if;
      
         when "101" =>   --the slave grabs our second byte, ext_tx_ready remains high but drops after this edge
           if ext_tx_ready='1' then   --we update the output to hold the slave's current data, but we can't ask for more yet since the MAC will stop here 
             tx_mux<="11"; --mux the slave directly to the MAC so that it can provide data on time once the MAC restarts (it already has a byte available, but once it sees a tx_ready it'll provide new bytes
             B_state<="110";      
           end if;
           
           
         when "110"  =>  
           if B_tx_valid='0' then --must only happen when after  error or last has been asserted
             tx_mux<="10";
             B_state<="000";
             B_tx_valid_int<='0';
             B_tx_request<='0';
           end if;
         when others => 
          null;
       end case; 
      
      
      
      

      if A_tx_request='0' then
        A_tx_grant<='0';
      end if;  

      if B_tx_request='0' then
        B_tx_grant<='0';
      end if;  

      if A_tx_grant='0' and B_tx_grant='0' then
        if A_tx_request='1' then
          A_tx_grant<='1';
        elsif B_tx_request='1' then
          B_tx_grant<='1';
        end if;   
      
      end if;


      if A_override='1' then
        tx_mux<="01";
      end if;
   end if; 
 end process;
 
 
 A_bus<=A_tx_last&A_tx_error&A_tx_data;
 B_bus<=B_tx_last&B_tx_error&B_tx_data;
 ext_tx_data<=ext_bus(7 downto 0);
 ext_tx_last<=ext_bus(9); 
 ext_tx_error<=ext_bus(8);
 
 mux: process(A0,B0,tx_mux,A_bus,B_bus,A_tx_valid,B_tx_valid,A_tx_valid_int,B_tx_valid_int,A_tx_ready_int,B_tx_ready_int,ext_tx_ready)
 begin 
   case tx_mux is
     when "00" => --buffered A-data
       ext_bus<=A0;
       ext_tx_valid<=A_tx_valid_int;
       
       A_tx_ready<=A_tx_ready_int;
       B_tx_ready<=B_tx_ready_int; 
     when "01" => --current A-data      
       ext_bus<=A_bus;
       ext_tx_valid<=A_tx_valid;
       
       A_tx_ready<=ext_tx_ready;
       B_tx_ready<=B_tx_ready_int;   
     when "10" => --buffered B-data
       ext_bus<=B0; 
       ext_tx_valid<=B_tx_valid_int;

       A_tx_ready<=A_tx_ready_int;
       B_tx_ready<=B_tx_ready_int;
     when "11" => --current B-data      
       ext_bus<=B_bus;
       ext_tx_valid<=B_tx_valid;
       
       A_tx_ready<=A_tx_ready_int; 
       B_tx_ready<=ext_tx_ready;
     when others =>
       null; 
  end case;     
 end process; 
end Behavioral;
