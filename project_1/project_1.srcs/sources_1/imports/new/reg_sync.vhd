
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;


entity reg_sync is
    Generic (width : integer ; auto : integer);
    Port ( clk_a : in STD_LOGIC;
           clk_b : in STD_LOGIC;
           in_a : in STD_LOGIC_VECTOR (width-1 downto 0);
           strobe_a : in STD_LOGIC;
           ready_a : out STD_LOGIC;
           out_b : inout STD_LOGIC_VECTOR (width-1 downto 0);
           strobe_b : out std_logic);
end reg_sync;

architecture Behavioral of reg_sync is

 signal buf : std_logic_vector(width-1 downto 0);
 signal strobe_a_b,strobe_a_b_i,strobe_b_a,strobe_b_a_i : std_logic;
 signal strobe_a0_b,strobe_a0_b_i,strobe_b0_a,strobe_b0_a_i : std_logic;
 signal not_ready : std_logic :='0';
 signal sm_a : std_logic_vector(1 downto 0) :="00";
 signal sm_b : std_logic_vector(1 downto 0) :="00"; 

 
begin
 
  ready_a<=(not strobe_a) or not_ready; 
  
  process(clk_a) 
  begin
    if rising_edge(clk_a) then
      strobe_b_a_i<=strobe_b_a;
      strobe_b0_a_i<=strobe_b0_a;
      case (sm_a) is 
        when "00" => 
          if strobe_a='1' or ((auto=1) and (buf/=in_a)) then
           not_ready<='1';
           sm_a<="01";
           buf <=in_a;
           strobe_a0_b<='1';
           
           
          end if;
        when "01" => 
          if strobe_b0_a_i='1' then
            strobe_a_b<='1';            
            strobe_a0_b<='0';
            sm_a<="10";
          end if;

        when "10" => 
          if strobe_b_a_i='1' then
            strobe_a_b<='0';
            sm_a<="11";
          end if;

        when "11" => 
          if strobe_b_a_i='0' then
            not_ready<='0';
            sm_a<="00";
          end if;

        when others => 
          null;          
      end case;
      
    end if;
  end process;
  
  process(clk_b) 
  begin
    if rising_edge(clk_b) then
        strobe_a_b_i<=strobe_a_b;
        strobe_a0_b_i<=strobe_a0_b;
        strobe_b<='0';
        case (sm_b) is 
          when "00" =>
            if strobe_a0_b_i='1' then
              strobe_b0_a<='1';
              sm_b<="01";
            end if;

            
          when "01" =>          
            if strobe_a_b_i='1' then
              sm_b<="10";
              strobe_b0_a<='0';
              strobe_b_a<='1';
              out_b<=buf;
              strobe_b<='1';
            end if;
         
           when others => 
            if strobe_a_b_i='0' then
              sm_b<="00";
              strobe_b_a<='0';
            end if;
        end case;
    end if;    
  end process;


end Behavioral;
