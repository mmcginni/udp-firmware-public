library ieee;
use ieee.std_logic_1164.all;

entity crc_test is
  port ( result : out std_logic_vector (31 downto 0));
end crc_test;

architecture behavioral of crc_test is

signal clk, rst,crc_en  : std_logic;

signal data_in : std_logic_vector(31 downto 0);
signal data_in_i : std_logic_vector(31 downto 0);
signal crc_out : std_logic_vector(31 downto 0);


component crc 
  port ( data_in_e : in std_logic_vector (31 downto 0);
    crc_en , rst, clk : in std_logic;
    crc_out : out std_logic_vector (31 downto 0));
end component;


-- the result should be 2B 03 3F 1A (reversed, little endian, according to: https://www.scadacore.com/tools/programming-calculators/online-checksum-calculator/
begin


 crc_i : crc
    port map (
     crc_en => crc_en,
     rst => rst,
     clk => clk,
     data_in_e => data_in_i,
     crc_out => crc_out );
     
     
--these transformations make the 32-bit vectors look the same as vectors pasted into e.g.    https://crccalc.com/  (the plain CRC-32 on this page should be correct for ethernet)
--however, since data(7:0) is transmited first, those bits should go into the data_in_i(24 to 31).
--for the output data it's less clear, but result(7 downto 0) should probably be transmitted first
   
     
-- gen_f : for I in 0 to 31 generate
--      result(I) <=not crc_out(31-I);
--   end generate  gen_f;     
 result <= crc_out;

-- gen_f2 : for I in 0 to 7 generate
--      data_in_i(I) <=data_in(7-I);
--      data_in_i(8+I) <=data_in(15-I);
--      data_in_i(16+I) <=data_in(23-I);
--      data_in_i(24+I) <=data_in(31-I);
--   end generate  gen_f2;     


-- gen_f2 : for I in 0 to 31 generate
--      data_in_i(I) <=data_in(31-I);
--   end generate  gen_f2;     


data_in_i<=data_in;
     


 test: process 
 begin
    rst<='1'; crc_en<='0'; clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
 
      
   rst<='0'; crc_en<='1';
--   data_in<=X"000AE6F0";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
--   data_in<=X"05A30012";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
--   data_in<=X"34567890";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
--   data_in<=X"08004500";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
   
--   data_in<=X"FFFFFFFe";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
  -- data_in<=X"FFFFFFFf";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;

--   data_in<=X"00000000";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
--   data_in<=X"00000000";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;


data_in<=X"52f75a10";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"f2f8c8f4";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"f860941e";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"00450008";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"14505600";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"01400040";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"a8c03d69";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"a8c00200";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"00080300";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"6d260e06";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"8d790100";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"00005ed6";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"8b770000";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"0000000d";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"11100000";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"15141312";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"19181716";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"1d1c1b1a";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"21201f1e";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"25242322";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"29282726";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"2d2c2b2a";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"31302f2e";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"35343332";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
data_in<=X"39383736";  clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;



   crc_en<='0';   clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
      crc_en<='0';   clk<='0';   wait for 50 ns;    clk<='1';    wait for 50 ns;
   
   
   
   wait for 1000 ns;
 
 end process;
 


end behavioral;