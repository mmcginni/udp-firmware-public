########################################################
set_property IOSTANDARD LVDS_25 [get_ports fabric_clk_p]
set_property PACKAGE_PIN AK18 [get_ports fabric_clk_p]
create_clock -period 24.000 -name fabric_clk [get_ports fabric_clk_p]
########################################################
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks eth_txoutclk] -group [get_clocks -include_generated_clocks osc125_a] -group [get_clocks -include_generated_clocks osc125_b] -group [get_clocks -include_generated_clocks fabric_clk]
########################################################
set_property BITSTREAM.CONFIG.OVERTEMPPOWERDOWN ENABLE [current_design]
########################################################
set_operating_conditions -airflow 0
set_operating_conditions -heatsink low
########################################################



#create_clock -period 2.075 -name clk480_c [get_ports fmc_l8_gbtclk0_p]
#create_clock -period 6.250 -name clk160_c [get_ports fmc_l12_gbtclk0_a_p]

#set_false_path -from [get_clocks clkout0] -to [get_clocks clk_320_clk_wiz_1]



#set_false_path -from [get_clocks -of_objects [get_pins usr/pdm_inst/cal_clk/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins sys/clocks/syspll_inst/plle2_adv_inst/CLKOUT0]]
