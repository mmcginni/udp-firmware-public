-------------------------------------------------------------------------------
-- File       : ten_gig_eth_mac_0_xgmii_if.vhd
-- Author     : Xilinx Inc.
-------------------------------------------------------------------------------
-- Description: This is the XGMII interface vhdl code for the
-- Ten Gigabit Etherent MAC.  It may also contain the Recieve
-- clock generation depending on configuration options when
-- generated.
-------------------------------------------------------------------------------
-- (c) Copyright 2001-2014 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
--
--
-------------------------------------------------------------------------------
library unisim;
use unisim.vcomponents.all;

library ieee;
use ieee.std_logic_1164.all;

entity ten_gig_eth_mac_0_xgmii_if is
    port (
    reset          : in std_logic;
    rx_axis_rstn   : in std_logic;
    tx_clk0        : in  std_logic;
    tx_clk90       : in  std_logic;
    xgmii_txd_core : in std_logic_vector(63 downto 0);
    xgmii_txc_core : in std_logic_vector(7 downto 0);
    xgmii_txd      : out std_logic_vector(31 downto 0);
    xgmii_txc      : out std_logic_vector(3 downto 0);
    xgmii_tx_clk   : out std_logic;

    rx_clk0        : out  std_logic;
    rx_dcm_locked  : out std_logic;
    xgmii_rx_clk   : in  std_logic;
    xgmii_rxd      : in  std_logic_vector(31 downto 0);
    xgmii_rxc      : in  std_logic_vector(3 downto 0);
    xgmii_rxd_core : out  std_logic_vector(63 downto 0);
    xgmii_rxc_core : out  std_logic_vector(7 downto 0));
end ten_gig_eth_mac_0_xgmii_if;


architecture wrapper of ten_gig_eth_mac_0_xgmii_if is

  constant D_LOCAL_FAULT : bit_vector(31 downto 0) := X"0100009C";
  constant C_LOCAL_FAULT : bit_vector(3 downto 0)  := "0001";
  -----------------------------------------------------------------------------
  -- Internal Signal Declaration for XGMAC (the 10Gb/E MAC core).
  -----------------------------------------------------------------------------

  signal xgmii_rx_clk_dcm : std_logic;
  signal rx_clk0_int : std_logic;
  signal rx_dcm_clk0 : std_logic;
  signal rxd_sdr : std_logic_vector(63 downto 0);
  signal rxc_sdr : std_logic_vector(7 downto 0);
  signal clkfbout         : std_logic;
  signal clkfbout_buf     : std_logic;
  signal rx_mmcm_locked   : std_logic;

  attribute INIT : string;
  attribute keep : string;
  attribute keep of rx_clk0 : signal is "true";
  attribute DDR_CLK_EDGE : string;

  function bit_to_string (
    constant b : bit)
    return string is
  begin  -- bit_to_string
    if b = '1' then
      return "1";
    else
      return "0";
    end if;
  end bit_to_string;


begin

  -- receive clock management
  --  Global input clock buffer for Receiver Clock
  xgmii_rx_clk_ibuf : IBUF
    port map (
      I => xgmii_rx_clk,
      O => xgmii_rx_clk_dcm);

  rx_mmcm : MMCME2_BASE
  generic map
    (DIVCLK_DIVIDE        => 1,
     CLKFBOUT_MULT_F      => 6.000,
     CLKOUT0_DIVIDE_F     => 6.000,
     CLKIN1_PERIOD        => 6.400,
     CLKFBOUT_PHASE       => 0.000,
     CLKOUT0_PHASE        => 180.000,
     CLKOUT0_DUTY_CYCLE   => 0.5,
     REF_JITTER1          => 0.010)
  port map (
     CLKFBOUT    => clkfbout,
     CLKOUT0     => rx_dcm_clk0,
     CLKIN1      => xgmii_rx_clk_dcm,
     LOCKED      => rx_mmcm_locked,
     CLKFBIN     => clkfbout_buf,
     PWRDWN      => '0',
     RST         => reset,
     CLKFBOUTB   => open,
     CLKOUT0B    => open,
     CLKOUT1     => open,
     CLKOUT1B    => open,
     CLKOUT2     => open,
     CLKOUT2B    => open,
     CLKOUT3     => open,
     CLKOUT3B    => open,
     CLKOUT4     => open,
     CLKOUT5     => open,
     CLKOUT6     => open);

  rx_dcm_locked <= rx_mmcm_locked;


  clkf_buf : BUFG
    port map
     (O => clkfbout_buf,
      I => clkfbout);

  rx_bufg : BUFG
    port map(
      I => rx_dcm_clk0,
      O => rx_clk0_int);

  rx_clk0 <= rx_clk0_int;


   ------------------------------------------------------------
   --  TRANSMIT OUTPUT CLOCK LOGIC
   ------------------------------------------------------------
   -- synthesis dont_touch=true
   tx_clk_ddr : ODDR
     generic map (
       DDR_CLK_EDGE => "SAME_EDGE")
     port map (
       Q  => xgmii_tx_clk,
       D1 => '1',
       D2 => '0',
       C  => tx_clk90,
       CE => '1',
       R  => '0',
       S  => '0');
   -- synthesis dont_touch=true

  ------------------------------------------------------------
  --  TRANSMIT OUTPUT DATA LOGIC                            --
  ------------------------------------------------------------
  gen_txd_ddr_out_regs :  for I in 0 to 31 generate
  begin
    txd_ddr: ODDR
      generic map (
        INIT => D_LOCAL_FAULT(I),
        DDR_CLK_EDGE => "SAME_EDGE")
      port map (
         Q  => xgmii_txd(I),
         D1 => xgmii_txd_core(I),
         D2 => xgmii_txd_core(I + 32),
         C  => tx_clk0,
         CE => '1',
         R  => '0',
         S  => '0');
   end generate;

  ------------------------------------------------------------
  --  TRANSMIT OUTPUT CONTROL LOGIC                         --
  ------------------------------------------------------------
  gen_txc_ddr_out_regs : for I in 0 to 3 generate
  begin
    txc_ddr: ODDR
      generic map (
        INIT => C_LOCAL_FAULT(I),
        DDR_CLK_EDGE => "SAME_EDGE")
      port map (
         Q  => xgmii_txc(I),
         D1 => xgmii_txc_core(I),
         D2 => xgmii_txc_core(I+4),
         C  => tx_clk0,
         CE => '1',
         R  => '0',
         S  => '0');
   end generate;

------------------------------------------------------------------------------
-- Input Double Data Rate registers
------------------------------------------------------------------------------
  gen_rxd_ddr_reg : for I in 0 to 31 generate
  begin
    rxd_ddr : IDDR
      generic map (
        DDR_CLK_EDGE => "SAME_EDGE")
      port map (
        Q1 => xgmii_rxd_core(I+32),
        Q2 => xgmii_rxd_core(I),
        C  => rx_clk0_int,
        CE => '1',
        D  => xgmii_rxd(I),
        R  => '0',
        S  => '0');
  end generate;

  gen_rxc_ddr_reg : for I in 0 to 3 generate
  begin
    rxc_ddr : IDDR
      generic map (
        DDR_CLK_EDGE => "SAME_EDGE")
      port map (
        Q1 => xgmii_rxc_core(I+4),
        Q2 => xgmii_rxc_core(I),
        C  => rx_clk0_int,
        CE => '1',
        D  => xgmii_rxc(I),
        R  => '0',
        S  => '0');
  end generate;


end wrapper;

