vlib work
vlib riviera

vlib riviera/xpm
vlib riviera/ten_gig_eth_mac_v15_1_7
vlib riviera/xil_defaultlib

vmap xpm riviera/xpm
vmap ten_gig_eth_mac_v15_1_7 riviera/ten_gig_eth_mac_v15_1_7
vmap xil_defaultlib riviera/xil_defaultlib

vlog -work xpm  -sv2k12 \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work ten_gig_eth_mac_v15_1_7  -v2k5 \
"../../../ipstatic/hdl/ten_gig_eth_mac_v15_1_rfs.v" \

vlog -work xil_defaultlib  -v2k5 \
"../../../ip/ten_gig_eth_mac_0/ten_gig_eth_mac_v15_1_1/hdl/ten_gig_eth_mac_0_core.v" \

vcom -work xil_defaultlib -93 \
"../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0_xgmii_if.vhd" \
"../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0_sync_resetn.vhd" \
"../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0_block.vhd" \
"../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

