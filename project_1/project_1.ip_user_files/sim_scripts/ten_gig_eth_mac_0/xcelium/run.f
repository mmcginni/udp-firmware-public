-makelib xcelium_lib/xpm -sv \
  "/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/ten_gig_eth_mac_v15_1_7 \
  "../../../ipstatic/hdl/ten_gig_eth_mac_v15_1_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../ip/ten_gig_eth_mac_0/ten_gig_eth_mac_v15_1_1/hdl/ten_gig_eth_mac_0_core.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0_xgmii_if.vhd" \
  "../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0_sync_resetn.vhd" \
  "../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0_block.vhd" \
  "../../../ip/ten_gig_eth_mac_0/synth/ten_gig_eth_mac_0.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

